#!/bin/bash

set -e

source loadenv.sh

buildDirectory="$(dirname "$(dirname "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )")")"
executable=$(find $buildDirectory -name bad_apple | head -1)
ffmpeg -i $(python3 -m yt_dlp -f 133 "https://www.youtube.com/watch?v=UkgK8eUdpAo" --get-url --rm-cache-dir) bad_apple_img%04d.png

ffmpeg -i $(python3 -m yt_dlp -f 133 "https://www.youtube.com/watch?v=UkgK8eUdpAo" --get-url --rm-cache-dir) bad_apple_img%04d.png

$directory/bad_apple $(pwd) --depth=12 --sampling-order=15

for f in bad_apple_img*.png; do
    rm $f
done