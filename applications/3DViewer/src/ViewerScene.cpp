// --- Graphics Includes ---
#include <ciegl/ciegl.hpp>

// --- Utility Includes ---
#include <cieutils/macros.hpp>
#include <cieutils/ranges.hpp>
#include "cieutils/cmake_variables.hpp"

// --- Internal Includes ---
#include "ViewerScene.hpp"

// --- STL Includes ---
#include <numeric>
#include <algorithm>
#include <filesystem>
#include <ranges>


namespace cie {


const std::filesystem::path SHADER_DIR = getDataPath() / "3DViewer/data/shaders";


ViewerScene::ViewerScene( const std::string& r_name,
                          utils::Logger& r_logger ) :
    gl::Scene(
        r_name,
        gl::makeVertexShader<gl::GLFWVertexShader>( SHADER_DIR / "vertexShader.json",
                                                    SHADER_DIR / "vertexShader.glsl" ),
        gl::makeGeometryShader<gl::GLFWGeometryShader>( SHADER_DIR / "geometryShader.json",
                                                        SHADER_DIR / "geometryShader.glsl" ),
        gl::makeFragmentShader<gl::GLFWFragmentShader>( SHADER_DIR / "fragmentShader.json",
                                                        SHADER_DIR / "fragmentShader.glsl" ),
        r_logger
    ),
    _models(),
    _updateModels(true)
{
    glEnable( GL_DEPTH_TEST );

    auto p_camera = this->makeCamera<gl::Camera<gl::PerspectiveProjection>>( r_logger );
    this->bindUniform( "transformation", p_camera->transformationMatrix() );
    this->bindUniform( "cameraPosition", p_camera->position() );
}


void ViewerScene::addModel( gl::PartPtr p_model )
{
    this->_models.push_back( p_model );
    this->_updateModels = true;
}


void ViewerScene::removeModel( gl::PartPtr p_model )
{
    auto it = std::find(
        this->_models.begin(),
        this->_models.end(),
        p_model
    );

    if ( it != this->_models.end() )
    {
        this->_models.erase( it );
        this->_updateModels = true;
    }
}


gl::CameraPtr ViewerScene::getCamera()
{
    return *this->_cameras.begin();
}


void ViewerScene::updateModels()
{
    // Allocate memory on the GPU
    Size attributeByteCount = 0;
    Size indexByteCount     = 0;
    Size triangleCount      = 0;
    for ( const auto& rp_model : this->_models )
    {
        attributeByteCount += rp_model->attributeByteCount();
        indexByteCount     += rp_model->indexByteCount();
        triangleCount      += rp_model->numberOfPrimitives();
    }

    if (auto p_buffer = this->_p_bufferManager->template getBoundBuffer<gl::VertexBuffer>().lock())
        p_buffer->resize(attributeByteCount);
    else
        CIE_THROW(Exception, "Attempt to write to destroyed buffer")

    if (auto p_buffer = this->_p_bufferManager->template getBoundBuffer<gl::ElementBuffer>().lock())
        p_buffer->resize(indexByteCount);
    else
        CIE_THROW(Exception, "Attempt to write to destroyed buffer")

    // Write to buffers
    Size attributeByteOffset = 0;
    Size indexByteOffset     = 0;
    Size indexOffset         = 0;

    for ( const auto& rp_model : this->_models )
    {
        this->_p_bufferManager->template writeToBoundBuffer<gl::VertexBuffer>(
            attributeByteOffset,
            rp_model->attributes()->begin(),
            rp_model->attributes()->size()
        );

        // Offset vertex indices
        auto offsetIndexRange = utils::makeTransformView<gl::Part::index_type>(
            rp_model->indices(),
            [indexOffset](auto index) { return index + indexOffset; }
        );

        this->_p_bufferManager->template writeToBoundBuffer<gl::ElementBuffer>(
            indexByteOffset,
            offsetIndexRange.begin(),
            offsetIndexRange.size()
        );

        attributeByteOffset += rp_model->attributeByteCount();
        indexByteOffset     += rp_model->indexByteCount();
        indexOffset         = rp_model->numberOfVertices();
    }

    this->_updateModels = false;
}


void ViewerScene::update_impl()
{
    CIE_BEGIN_EXCEPTION_TRACING

    if ( this->_updateModels )
        this->updateModels();

    GLint64 numberOfIndices;
    glGetBufferParameteri64v( GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &numberOfIndices );
    numberOfIndices /= sizeof( GLuint );
    glDrawElements( GL_TRIANGLES, numberOfIndices, GL_UNSIGNED_INT, 0 );

    CIE_END_EXCEPTION_TRACING
}


} // namespace cie