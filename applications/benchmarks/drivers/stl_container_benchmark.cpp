// --- Utility Includes ---
#include "cieutils/stl_extension.hpp"
#include "cieutils/logging.hpp"
#include "cieutils/cmake_variables.hpp"


namespace cie {


template <class ContainerType>
int testContainer( const ContainerType& r_container,
                   const std::string& r_containerName,
                   utils::Logger& r_log )
{
    auto block = r_log.newBlock( r_containerName );
    const Size numberOfIterations = 1e6;

    std::stringstream stream;

    // Copy constructor and assignment
    {
        auto localBlock = r_log.newBlock( "copy and assign" );

        ContainerType container( r_container );
        for ( Size i=0; i<numberOfIterations; ++i )
            container = ContainerType( r_container );

        stream << container[0];
    }

    // Component access
    {
        auto localBlock = r_log.newBlock( "component access" );
        typename ContainerType::value_type dummy = 0;
        for ( Size i=0; i<numberOfIterations; ++i )
            for ( const auto& r_component : r_container )
                dummy += r_component;

        stream << dummy;
    }

    return stream.str().size();
}


int main()
{
    utils::Logger& log = cie::utils::LoggerSingleton::get(getOutputPath() / "stl_container_benchmark.log");

    const Size smallArraySize  = 3;
    const Size mediumArraySize = 500;
    const Size largeArraySize  = 1e5;

    auto run = [&log]( Size arraySize )
    {
        auto block = log.newBlock( "array size = " + std::to_string(arraySize) );
        std::vector<int> vector( arraySize );
        DynamicArray<int> dynamicArray( arraySize );

        Size dummy = 0;
        dummy += testContainer( vector, "std::vector", log );
        dummy += testContainer( dynamicArray, "cie::DynamicArray", log );

        return dummy;
    };

    Size dummy = 0;
    dummy += run( smallArraySize );
    dummy += run( mediumArraySize );
    dummy += run( largeArraySize );

    return dummy;
}


} // namespace cie


int main()
{
    return cie::main();
}