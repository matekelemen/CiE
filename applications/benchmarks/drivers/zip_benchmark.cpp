// --- Utility Includes ---
#include "cieutils/packages/stl_extension/inc/zip.hpp"
#include "cieutils/packages/stl_extension/inc/DynamicArray.hpp"
#include "cieutils/packages/logging/inc/LoggerSingleton.hpp"
#include "cieutils/packages/logging/inc/LogBlock.hpp"
#include "cieutils/packages/types/inc/Color.hpp"
#include "cieutils/cmake_variables.hpp"

// --- STL Includes ---
#include <string>


int main()
{
    auto& r_log = cie::utils::LoggerSingleton::get(cie::getOutputPath() / "zip_benchmark.log");

    constexpr cie::Size size = 1e7;
    cie::DynamicArray<int> ints(size);
    cie::DynamicArray<float> floats(size);
    cie::DynamicArray<double> doubles(size);
    cie::DynamicArray<std::string> strings(size);

    {
        auto block = r_log.newBlock("iterator-based for");

        auto it_int = ints.begin();
        auto it_float = floats.begin();
        auto it_double = doubles.begin();
        auto it_string = strings.begin();

        auto it_end = ints.end();

        for (; it_int!=it_end; ++it_int, ++it_float, ++it_double, ++it_string)
        {
            *it_int = 2;
            *it_float = 2.0;
            *it_double = 2.0;
            *it_string = "god yzal eht revo spmuj xof nworb kciuq ehT";
        }
    }

    {
        auto block = r_log.newBlock("zip");

        for (auto&& [i, f, d, s] : cie::utils::zip(ints, floats, doubles, strings))
        {
            i = 1;
            f = 1.0;
            d = 1.0;
            s = "The quick brown fox jumps over the lazy dog";
        }
    }

    // Compute something unnecessary to
    // avoid compiler optimizations
    int result = 0;
    for (cie::Size i=0; i<size; ++i)
    {
        result += ints[i];
        result -= floats[i];
        result += doubles[i];
        result -= strings[i].size() - 43 + i%2;
    }

    return result == 0 ? 0 : 1;
}
