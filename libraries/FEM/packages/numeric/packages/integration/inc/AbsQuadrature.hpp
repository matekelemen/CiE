#ifndef CIE_FEM_NUMERIC_ABS_QUADRATURE_HPP
#define CIE_FEM_NUMERIC_ABS_QUADRATURE_HPP

// --- Utility Includes ---
#include "cieutils/packages/stl_extension/inc/DynamicArray.hpp"

// --- STL Includes ---
#include <utility>


namespace cie::fem {


template <concepts::Numeric NT>
class AbsQuadrature
{
public:
    using NodeContainer   = DynamicArray<NT>;
    using WeightContainer = DynamicArray<NT>;

public:
    virtual ~AbsQuadrature()
    {}

    Size numberOfNodes() const;

    Size order() const;

    const NodeContainer& nodes() const;

    const WeightContainer& weights() const;

protected:
    AbsQuadrature( NodeContainer&& r_nodes,
                   WeightContainer&& r_weights );

    AbsQuadrature( std::pair<NodeContainer, WeightContainer>&& r_nodesAndWeights );

    AbsQuadrature( const NodeContainer& r_nodes,
                   const WeightContainer& r_weights );

private:
    NodeContainer   _nodes;
    WeightContainer _weights;
};


} // namespace cie::fem

#endif