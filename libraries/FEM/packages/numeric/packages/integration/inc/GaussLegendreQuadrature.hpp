#ifndef CIE_FEM_NUMERIC_GAUSS_LEGENDRE_QUADRATURE_HPP
#define CIE_FEM_NUMERIC_GAUSS_LEGENDRE_QUADRATURE_HPP

// --- Internal Includes ---
#include "FEM/packages/numeric/packages/integration/inc/AbsQuadrature.hpp"


namespace cie::fem {


template <concepts::Numeric NT>
class GaussLegendreQuadrature final : public AbsQuadrature<NT>
{
public:
    using typename AbsQuadrature<NT>::NodeContainer;
    using typename AbsQuadrature<NT>::WeightContainer;

public:
    GaussLegendreQuadrature(Size integrationOrder,
                            NT maxAbsoluteNodeError  = 1e-10,
                            Size maxNewtonIterations = 10);

private:
    class Initializer
    {
    public:
        static std::pair<NodeContainer,WeightContainer> getNodesAndWeights(Size integrationOrder,
                                                                           NT maxAbsoluteError,
                                                                           Size maxIterations);

        static NT approximateLegendreRoot(Size order, Size index);
    }; // class Initializer
}; // class GaussLegendreQuadrature


} // namespace cie::fem

//#include "FEM/packages/numeric/packages/integration/impl/GaussLegendreQuadrature_impl.hpp"

#endif