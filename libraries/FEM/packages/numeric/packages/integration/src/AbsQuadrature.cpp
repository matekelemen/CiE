// --- Utility Includes ---
#include "cieutils/packages/macros/inc/exceptions.hpp"

// --- FEM Includes ---
#include "FEM/packages/utilities/inc/template_macros.hpp"

// --- Internal Includes ---
#include "FEM/packages/numeric/packages/integration/inc/AbsQuadrature.hpp"


namespace cie::fem {


template <concepts::Numeric NT>
AbsQuadrature<NT>::AbsQuadrature( typename AbsQuadrature<NT>::NodeContainer&& r_nodes,
                                  typename AbsQuadrature<NT>::WeightContainer&& r_weights )
    : _nodes( std::move(r_nodes) ),
      _weights( std::move(r_weights) )
{
}


template <concepts::Numeric NT>
AbsQuadrature<NT>::AbsQuadrature( std::pair<typename AbsQuadrature<NT>::NodeContainer, typename AbsQuadrature<NT>::WeightContainer>&& r_nodesAndWeights )
    : _nodes( std::move(r_nodesAndWeights.first) ),
      _weights( std::move(r_nodesAndWeights.second) )
{
}


template <concepts::Numeric NT>
AbsQuadrature<NT>::AbsQuadrature( const typename AbsQuadrature<NT>::NodeContainer& r_nodes,
                                  const typename AbsQuadrature<NT>::WeightContainer& r_weights )
    : _nodes( r_nodes ),
      _weights( r_weights )
{
}


template <concepts::Numeric NT>
Size AbsQuadrature<NT>::numberOfNodes() const
{
    return _nodes.size();
}


template <concepts::Numeric NT>
Size AbsQuadrature<NT>::order() const
{
    return _nodes.size();
}


template <concepts::Numeric NT>
const typename AbsQuadrature<NT>::NodeContainer&
AbsQuadrature<NT>::nodes() const
{
    return _nodes;
}


template <concepts::Numeric NT>
const typename AbsQuadrature<NT>::WeightContainer&
AbsQuadrature<NT>::weights() const
{
    return _weights;
}


CIE_FEM_INSTANTIATE_NUMERIC_TEMPLATE(AbsQuadrature)


} // namespace cie::fem