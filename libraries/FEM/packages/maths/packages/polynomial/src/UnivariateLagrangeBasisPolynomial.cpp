// --- FEM Includes ---
#include "FEM/packages/utilities/inc/template_macros.hpp"

// --- Internal Includes ---
#include "FEM/packages/maths/packages/polynomial/inc/UnivariateLagrangeBasisPolynomial.hpp"


namespace cie::fem::maths {


template <concepts::Numeric NT>
UnivariateLagrangeBasisPolynomial<NT>::UnivariateLagrangeBasisPolynomial( std::initializer_list<NT>&& r_nodes,
                                                                          Size basisIndex ) :
    UnivariateLagrangeBasisPolynomial<NT>( std::vector<NT>(std::move(r_nodes)), basisIndex )
{
}


CIE_FEM_INSTANTIATE_NUMERIC_TEMPLATE(UnivariateLagrangeBasisPolynomial)


} // namespace cie::fem::maths