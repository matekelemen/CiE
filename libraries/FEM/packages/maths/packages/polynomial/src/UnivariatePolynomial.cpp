// --- External Includes ---
#include <Eigen/Polynomials>

// --- Utility Includes ---
#include "cieutils/packages/macros/inc/exceptions.hpp"
#include "cieutils/packages/stl_extension/inc/resize.hpp"

// --- FEM Incldues ---
#include "FEM/packages/utilities/inc/template_macros.hpp"

// --- Internal Includes ---
#include "FEM/packages/maths/packages/polynomial/inc/UnivariatePolynomial.hpp"


namespace cie::fem::maths {


template <concepts::Numeric NT>
UnivariatePolynomial<NT>::UnivariatePolynomial( typename UnivariatePolynomial<NT>::coefficient_container&& r_coefficients ) :
    _coefficients( std::move(r_coefficients) )
{
}


template <concepts::Numeric NT>
UnivariatePolynomial<NT>::UnivariatePolynomial( const typename UnivariatePolynomial<NT>::coefficient_container& r_coefficients ) :
    _coefficients( r_coefficients )
{
}


template <concepts::Numeric NT>
UnivariatePolynomial<NT>::UnivariatePolynomial( typename UnivariatePolynomial<NT>::coefficient_list&& r_coefficients ) :
    _coefficients( std::move(r_coefficients) )
{
}


template <concepts::Numeric NT>
typename UnivariatePolynomial<NT>::value_type
UnivariatePolynomial<NT>::operator()(NT argument) const
{
    CIE_BEGIN_EXCEPTION_TRACING

    auto order = _coefficients.size();

    if (order > 0) [[likely]]
        return Eigen::poly_eval(_coefficients, argument);
    else [[unlikely]]
        return 0;

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Numeric NT>
typename UnivariatePolynomial<NT>::derivative_ptr
UnivariatePolynomial<NT>::derivative() const
{
    CIE_BEGIN_EXCEPTION_TRACING

    typename UnivariatePolynomial<NT>::coefficient_container derivativeCoefficients;

    Size polynomialOrder = this->_coefficients.size();

    if ( 1 < polynomialOrder )
    {
        utils::resize( derivativeCoefficients, polynomialOrder - 1 );

        const NT* p_coefficient = &this->_coefficients[1];
        NT* p_derivativeCoefficient = &derivativeCoefficients[0];

        for ( Size power=1; power<polynomialOrder; ++power )
            *p_derivativeCoefficient++ = power * (*p_coefficient++);
    }

    return typename UnivariatePolynomial<NT>::derivative_ptr(
        new UnivariatePolynomial<NT>( std::move(derivativeCoefficients) )
    );

    CIE_END_EXCEPTION_TRACING
}


CIE_FEM_INSTANTIATE_NUMERIC_TEMPLATE(UnivariatePolynomial)


} // namespace cie::fem::maths
