// --- Utility Includes ---
#include "cieutils/packages/testing/inc/essentials.hpp"

// --- Internal Includes ---
#include "FEM/packages/maths/packages/operation/inc/Product.hpp"
#include "FEM/packages/maths/inc/SeparableScalarFunction.hpp"
#include "FEM/packages/maths/packages/polynomial/inc/UnivariatePolynomial.hpp"


namespace cie::fem::maths {


CIE_TEST_CASE("makeProduct: scalar * scalar", "[maths]")
{
    {
        CIE_TEST_CASE_INIT("makeProduct 1D: scalar * scalar")

        using Univariate = UnivariateScalarFunction<double>::FunctionBase;

        auto p_p0 = Univariate::SharedPointer(new UnivariatePolynomial<double>({1.0, -1.0}));
        auto p_p1 = Univariate::SharedPointer(new UnivariatePolynomial<double>({1.0, 1.0}));

        using Basis = ScalarFunction<1,double>;

        auto p_f0 = Basis::SharedPointer(
            new SeparableScalarFunction<1,double>({p_p0})
        );

        auto p_f1 = Basis::SharedPointer(
            new SeparableScalarFunction<1,double>({p_p1})
        );

        // Create function (1-x)*(1+x)
        Basis::FunctionBase::SharedPointer p_product;
        CIE_TEST_REQUIRE_NOTHROW(p_product = makeProduct(p_f0, p_f1));

        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {-5.0}) == Approx(-24.0));
        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {-2.0}) == Approx(-3.0));
        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {-1.0}) == Approx(0.0));
        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {0.0}) == Approx(1.0));
        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {1.0}) == Approx(0.0));
        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {2.0}) == Approx(-3.0));
        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {5.0}) == Approx(-24.0));
    }

    {
        CIE_TEST_CASE_INIT("makeProduct 2D: scalar * scalar")

        using Univariate = UnivariateScalarFunction<double>::FunctionBase;

        auto p_p00 = Univariate::SharedPointer(new UnivariatePolynomial<double>({1.0, -1.0}));
        auto p_p01 = Univariate::SharedPointer(new UnivariatePolynomial<double>({1.0, 1.0}));
        auto p_p10 = Univariate::SharedPointer(new UnivariatePolynomial<double>({0.0, 0.0, 1.0}));
        auto p_p11 = Univariate::SharedPointer(new UnivariatePolynomial<double>({0.0, 0.0, 1.0}));

        using Basis = ScalarFunction<2,double>;

        auto p_f0 = Basis::SharedPointer(
            new SeparableScalarFunction<2,double>({p_p00, p_p01})
        );

        auto p_f1 = Basis::SharedPointer(
            new SeparableScalarFunction<2,double>({p_p10, p_p11})
        );

        // Create function (1-x)*(1+x)
        Basis::FunctionBase::SharedPointer p_product;
        CIE_TEST_REQUIRE_NOTHROW(p_product = makeProduct(p_f0, p_f1));

        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {-2.0, -3.0}) == Approx(-216.0));
        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {2.0, -3.0}) == Approx(72.0));
        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {-2.0, 3.0}) == Approx(432.0));
        CIE_TEST_CHECK(p_product->evaluate(Basis::argument_type {2.0, 3.0}) == Approx(-144.0));

        // Derivative
        auto p_derivative = p_product->derivative();

        auto derivative = p_derivative->evaluate(Basis::argument_type {-2.0, -3.0});
        CIE_TEST_CHECK(derivative[0] == Approx(288.0));
        CIE_TEST_CHECK(derivative[1] == Approx(252.0));

        derivative = p_derivative->evaluate(Basis::argument_type {2.0, -3.0});
        CIE_TEST_CHECK(derivative[0] == Approx(144));
        CIE_TEST_CHECK(derivative[1] == Approx(-84.0));

        derivative = p_derivative->evaluate(Basis::argument_type {-2.0, 3.0});
        CIE_TEST_CHECK(derivative[0] == Approx(-576.0));
        CIE_TEST_CHECK(derivative[1] == Approx(396.0));

        derivative = p_derivative->evaluate(Basis::argument_type {2.0, 3.0});
        CIE_TEST_CHECK(derivative[0] == Approx(-288.0));
        CIE_TEST_CHECK(derivative[1] == Approx(-132.0));

        // Second derivative
        auto p_secondDerivative = p_derivative->derivative();

        auto secondDerivative = p_secondDerivative->evaluate(Basis::argument_type {-2.0, -3.0});
        CIE_TEST_CHECK(secondDerivative(0,0) == Approx(-252.0));
        CIE_TEST_CHECK(secondDerivative(1,0) == Approx(-396.0));
        CIE_TEST_CHECK(secondDerivative(0,1) == Approx(-276.0));
        CIE_TEST_CHECK(secondDerivative(1,1) == Approx(-192.0));

        secondDerivative = p_secondDerivative->evaluate(Basis::argument_type {2.0, -3.0});
        CIE_TEST_CHECK(secondDerivative(0,0) == Approx(180.0));
        CIE_TEST_CHECK(secondDerivative(1,0) == Approx(-156.0));
        CIE_TEST_CHECK(secondDerivative(0,1) == Approx(-180.0));
        CIE_TEST_CHECK(secondDerivative(1,1) == Approx(64.0));

        secondDerivative = p_secondDerivative->evaluate(Basis::argument_type {-2.0, 3.0});
        CIE_TEST_CHECK(secondDerivative(0,0) == Approx(504.0));
        CIE_TEST_CHECK(secondDerivative(1,0) == Approx(-540.0));
        CIE_TEST_CHECK(secondDerivative(0,1) == Approx(-516.0));
        CIE_TEST_CHECK(secondDerivative(1,1) == Approx(240.0));

        secondDerivative = p_secondDerivative->evaluate(Basis::argument_type {2.0, 3.0});
        CIE_TEST_CHECK(secondDerivative(0,0) == Approx(-360.0));
        CIE_TEST_CHECK(secondDerivative(1,0) == Approx(-204.0));
        CIE_TEST_CHECK(secondDerivative(0,1) == Approx(-324.0));
        CIE_TEST_CHECK(secondDerivative(1,1) == Approx(-80.0));
    }
}


CIE_TEST_CASE("makeProduct: scalar * vector", "[maths]")
{
    CIE_TEST_CASE_INIT("makeProduct: scalar * vector")

    using Univariate    = UnivariateScalarFunction<float>;
    using ScalarOperand = ScalarFunction<3, float>;
    using VectorOperand = VectorFunction<2, 3, float>;

    Univariate::SharedPointer p_p0(new UnivariatePolynomial<float>({1, -1}));
    Univariate::SharedPointer p_p1(new UnivariatePolynomial<float>({1, 1}));
    Univariate::SharedPointer p_p2(new UnivariatePolynomial<float>({-5, -5}));

    auto p_scalarOperand = ScalarOperand::SharedPointer(
        new SeparableScalarFunction<3, float>({p_p0, p_p1, p_p2})
    );

    auto p_vectorComponent0 = SeparableScalarFunction<3, float>::SharedPointer(
        new SeparableScalarFunction<3, float>({p_p0, p_p1, p_p2})
    );

    auto p_vectorComponent1 = SeparableScalarFunction<3, float>::SharedPointer(
        new SeparableScalarFunction<3, float>({p_p2, p_p1, p_p0})
    );

    auto p_vectorOperand = VectorOperand::SharedPointer(
        new SeparableVectorFunction<2, 3, float>({
            p_vectorComponent0,
            p_vectorComponent1
        })
    );
}


} // namespace cie::fem::maths