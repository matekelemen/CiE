#ifndef CIE_FEM_MATHS_OPERATION_SUM_IMPL_HPP
#define CIE_FEM_MATHS_OPERATION_SUM_IMPL_HPP

// --- Utility Includes ---
#include "cieutils/packages/macros/inc/exceptions.hpp"
#include "cieutils/packages/macros/inc/warning.hpp"

// --- Linalg Includes ---
#include "linalg/packages/overloads/inc/vector_operators.hpp"
#include "linalg/packages/overloads/inc/matrix_operators.hpp"


namespace cie::fem::maths {


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
inline typename Sum<LHS,RHS,Result>::value_type
Sum<LHS,RHS,Result>::operator()(const typename Sum<LHS,RHS,Result>::argument_type& r_argument) const
{
    CIE_BEGIN_EXCEPTION_TRACING
    return this->lhs()->evaluate(r_argument) + this->rhs()->evaluate(r_argument);
    CIE_END_EXCEPTION_TRACING
}


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
typename Sum<LHS,RHS,Result>::derivative_ptr
Sum<LHS,RHS,Result>::derivative() const
{
    CIE_BEGIN_EXCEPTION_TRACING

    using LHSDerivative = typename LHS::derivative_type::FunctionBase;
    using RHSDerivative = typename RHS::derivative_type::FunctionBase;

    return makeSum(
        std::static_pointer_cast<LHSDerivative>(this->lhs()->derivative()),
        std::static_pointer_cast<RHSDerivative>(this->rhs()->derivative())
    );

    CIE_END_EXCEPTION_TRACING
}


template <concepts::AbsFunctionPtr LHSPtr, concepts::AbsFunctionPtr RHSPtr>
typename detail::SumTraits<typename std::decay<LHSPtr>::type::element_type::FunctionBase,
                           typename std::decay<RHSPtr>::type::element_type::FunctionBase>::Result::SharedPointer
makeSum(LHSPtr&& rp_lhs, RHSPtr&& rp_rhs)
{
    CIE_BEGIN_EXCEPTION_TRACING

    using LHS = typename std::decay<LHSPtr>::type::element_type::FunctionBase;
    using RHS = typename std::decay<RHSPtr>::type::element_type::FunctionBase;
    using Result = detail::SumTraits<LHS,RHS>::Result;

    return typename Result::SharedPointer(new Sum<LHS,RHS,Result>(rp_lhs, rp_rhs));

    CIE_END_EXCEPTION_TRACING
}


} // namespace cie::fem::maths


#endif