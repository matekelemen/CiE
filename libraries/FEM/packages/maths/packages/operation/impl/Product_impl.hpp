#ifndef CIE_FEM_MATHS_OPERATOR_PRODUCT_IMPL_HPP
#define CIE_FEM_MATHS_OPERATOR_PRODUCT_IMPL_HPP

// --- Utility Includes ---
#include "cieutils/packages/macros/inc/checks.hpp"
#include "cieutils/packages/macros/inc/exceptions.hpp"

// --- Internal Includes ---
#include "FEM/packages/maths/packages/operation/inc/Sum.hpp"
#include "FEM/packages/maths/packages/operation/inc/GenericOperation.hpp"


namespace cie::fem::maths {


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
inline typename Product<LHS,RHS,Result>::value_type
Product<LHS,RHS,Result>::operator()(const typename Product<LHS,RHS,Result>::argument_type& r_argument) const
{
    CIE_BEGIN_EXCEPTION_TRACING
    return this->lhs()->evaluate(r_argument) * this->rhs()->evaluate(r_argument);
    CIE_END_EXCEPTION_TRACING
}


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
typename Product<LHS,RHS,Result>::derivative_ptr
Product<LHS,RHS,Result>::derivative() const
{
    CIE_BEGIN_EXCEPTION_TRACING

    using LHSDerivative = typename LHS::derivative_type::FunctionBase;
    using RHSDerivative = typename RHS::derivative_type::FunctionBase;

    auto p_lhs           = std::static_pointer_cast<typename LHS::FunctionBase>(this->lhs());
    auto p_rhs           = std::static_pointer_cast<typename RHS::FunctionBase>(this->rhs());
    auto p_lhsDerivative = std::static_pointer_cast<LHSDerivative>(this->lhs()->derivative());
    auto p_rhsDerivative = std::static_pointer_cast<RHSDerivative>(this->rhs()->derivative());

    return makeSum(
        makeProduct(p_lhsDerivative, p_rhs),
        makeProduct(p_lhs, p_rhsDerivative)
    );
    CIE_END_EXCEPTION_TRACING
}


template <concepts::VectorFunction VF>
typename detail::ProductTraits<VF,VF>::Result::SharedPointer
detail::ProductTraits<VF,VF>::makeProduct(const typename VF::SharedPointer& rp_lhs,
                                          const typename VF::SharedPointer& rp_rhs)
{
    CIE_BEGIN_EXCEPTION_TRACING

    using Argument   = typename Result::argument_type;

    auto operation = [rp_lhs, rp_rhs](const Argument& r_argument) -> typename Result::value_type
    {
        CIE_BEGIN_EXCEPTION_TRACING

        typename Result::value_type matrix;
        constexpr Size size = VF::value_type::array_size;

        CIE_OUT_OF_RANGE_CHECK(matrix.rowSize() == size)
        CIE_OUT_OF_RANGE_CHECK(matrix.columnSize() == size)

        auto lhs = rp_lhs->evaluate(r_argument);
        auto rhs = rp_rhs->evaluate(r_argument);

        for (Size i_lhs=0; i_lhs<size; ++i_lhs)
            for (Size i_rhs=0; i_rhs<size; ++i_rhs)
                matrix(i_lhs, i_rhs) = lhs[i_lhs] * rhs[i_rhs];

        return matrix;

        CIE_END_EXCEPTION_TRACING
    };

    return typename Result::SharedPointer(new GenericOperation<Result>(
        operation
    ));

    CIE_END_EXCEPTION_TRACING
}


template <concepts::VectorFunction VF, concepts::MatrixFunction MF>
requires (VF::value_type::array_size == MF::value_type::RowTag)
typename detail::ProductTraits<VF,MF>::Result::SharedPointer
detail::ProductTraits<VF,MF>::makeProduct(const typename VF::SharedPointer& rp_lhs,
                                          const typename MF::SharedPointer& rp_rhs)
{
    CIE_BEGIN_EXCEPTION_TRACING

    using Argument = typename Result::argument_type;

    auto operation = [rp_lhs, rp_rhs](const Argument& r_argument) -> typename Result::value_type
    {
        CIE_BEGIN_EXCEPTION_TRACING

        typename Result::value_type matrix;

        auto lhs = rp_lhs->evaluate(r_argument);
        auto rhs = rp_rhs->evaluate(r_argument);

        // Transpose in place
        Eigen::Map<Eigen::Matrix<typename Result::value_type::value_type,1,VF::value_type::array_size>> transpose(&lhs[0]);

        return transpose * rhs;

        CIE_END_EXCEPTION_TRACING
    };

    return typename Result::SharedPointer(new GenericOperation<Result>(
        operation
    ));

    CIE_END_EXCEPTION_TRACING
}


template <concepts::AbsFunctionPtr LHSPtr, concepts::AbsFunctionPtr RHSPtr>
requires concepts::detail::HasSpecialProduct<typename std::decay<LHSPtr>::type::element_type,
                                             typename std::decay<RHSPtr>::type::element_type>
typename detail::ProductTraits<typename std::decay<LHSPtr>::type::element_type,
                               typename std::decay<RHSPtr>::type::element_type>::Result::SharedPointer
makeProduct(LHSPtr&& rp_lhs, RHSPtr&& rp_rhs)
{
    CIE_BEGIN_EXCEPTION_TRACING
    return detail::ProductTraits<typename std::decay<LHSPtr>::type::element_type,
                                 typename std::decay<RHSPtr>::type::element_type>::makeProduct(rp_lhs, rp_rhs);
    CIE_END_EXCEPTION_TRACING
}



template <concepts::AbsFunctionPtr LHSPtr, concepts::AbsFunctionPtr RHSPtr>
requires (!concepts::detail::HasSpecialProduct<typename std::decay<LHSPtr>::type::element_type,
                                               typename std::decay<RHSPtr>::type::element_type>)
typename detail::ProductTraits<typename std::decay<LHSPtr>::type::element_type,
                               typename std::decay<RHSPtr>::type::element_type>::Result::SharedPointer
makeProduct(LHSPtr&& rp_lhs, RHSPtr&& rp_rhs)
    CIE_BEGIN_EXCEPTION_TRACING

    using LHS = typename std::decay<LHSPtr>::type::element_type::FunctionBase;
    using RHS = typename std::decay<RHSPtr>::type::element_type::FunctionBase;
    using Result = typename detail::ProductTraits<typename std::decay<LHSPtr>::type::element_type,
                                                  typename std::decay<RHSPtr>::type::element_type>::Result;

    return typename Result::SharedPointer(new Product<LHS,RHS,Result>(rp_lhs, rp_rhs));

    CIE_END_EXCEPTION_TRACING


} // namespace cie::fem::maths


#endif