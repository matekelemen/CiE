#ifndef CIE_FEM_MATHS_BINARY_OPERATOR_IMPL_HPP
#define CIE_FEM_MATHS_BINARY_OPERATOR_IMPL_HPP


namespace cie::fem::maths {


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
BinaryOperator<LHS,RHS,Result>::BinaryOperator(typename LHS::SharedPointer&& rp_lhs,
                                               typename RHS::SharedPointer&& rp_rhs)
    : _p_lhs(rp_lhs),
      _p_rhs(rp_rhs)
{
}


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
BinaryOperator<LHS,RHS,Result>::BinaryOperator(const typename LHS::SharedPointer& rp_lhs,
                                               const typename RHS::SharedPointer& rp_rhs)
    : _p_lhs(rp_lhs),
      _p_rhs(rp_rhs)
{
}


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
inline const typename LHS::SharedPointer&
BinaryOperator<LHS,RHS,Result>::lhs() const
{
    return _p_lhs;
}


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
inline const typename RHS::SharedPointer&
BinaryOperator<LHS,RHS,Result>::rhs() const
{
    return _p_rhs;
}


} // namespace cie::fem::maths


#endif