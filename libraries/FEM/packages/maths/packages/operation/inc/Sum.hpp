#ifndef CIE_FEM_MATHS_OPERATION_SUM_HPP
#define CIE_FEM_MATHS_OPERATION_SUM_HPP

// --- Internal Includes ---
#include "FEM/packages/maths/inc/MatrixFunction.hpp"
#include "FEM/packages/maths/inc/VectorFunction.hpp"
#include "FEM/packages/maths/inc/ScalarFunction.hpp"
#include "FEM/packages/maths/packages/operation/inc/BinaryOperator.hpp"
#include "FEM/packages/maths/inc/NotImplementedFunction.hpp"


namespace cie::fem::maths {


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
class Sum : public BinaryOperator<LHS,RHS,Result>
{
private:
    using Base = BinaryOperator<LHS,RHS,Result>;

public:
    using typename Base::value_type;
    using typename Base::argument_type;
    using typename Base::derivative_ptr;

    CIE_DEFINE_CLASS_POINTERS(Sum)

public:
    using Base::Base;

    using Base::operator=;

    virtual value_type operator()(const argument_type& r_argument) const override;

    virtual derivative_ptr derivative() const override;
}; // class Sum


namespace detail {

template <class LHS, class RHS>
struct SumTraits {};

template <concepts::ImplementedFunction LHS, concepts::NotImplementedFunction RHS>
struct SumTraits<LHS,RHS>
{using Result = NotImplementedFunction;};

template <concepts::NotImplementedFunction LHS, concepts::ImplementedFunction RHS>
struct SumTraits<LHS,RHS>
{using Result = NotImplementedFunction;};

template <concepts::NotImplementedFunction LHS, concepts::NotImplementedFunction RHS>
struct SumTraits<LHS,RHS>
{using Result = NotImplementedFunction;};

template <concepts::ScalarFunction Operand>
struct SumTraits<Operand,Operand>
{
    using Result = ScalarFunction<Operand::argument_type::array_size,
                                  typename Operand::value_type,
                                  typename Operand::derivative_type>;
};

template <concepts::VectorFunction Operand>
struct SumTraits<Operand,Operand>
{
    using Result = VectorFunction<Operand::value_type::array_size,
                                  Operand::argument_type::array_size,
                                  typename Operand::argument_type::value_type,
                                  typename Operand::derivative_type>;
};

template <concepts::MatrixFunction Operand>
struct SumTraits<Operand,Operand>
{
    using Result = MatrixFunction<Operand::value_type::RowTag,
                                  Operand::value_type::ColumnTag,
                                  Operand::argument_type::array_size,
                                  typename Operand::value_type::value_type,
                                  typename Operand::derivative_type>;
};

} // namespace detail


template <concepts::AbsFunctionPtr LHSPtr, concepts::AbsFunctionPtr RHSPtr>
typename detail::SumTraits<typename std::decay<LHSPtr>::type::element_type::FunctionBase,
                           typename std::decay<RHSPtr>::type::element_type::FunctionBase>::Result::SharedPointer
makeSum(LHSPtr&& rp_lhs, RHSPtr&& rp_rhs);


} // namespace cie::fem::maths

#include "FEM/packages/maths/packages/operation/impl/Sum_impl.hpp"

#endif