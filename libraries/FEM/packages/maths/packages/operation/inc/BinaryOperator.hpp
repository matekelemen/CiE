#ifndef CIE_FEM_MATHS_BINARY_OPERATOR_HPP
#define CIE_FEM_MATHS_BINARY_OPERATOR_HPP

// --- FEM Includes ---
#include "FEM/packages/maths/inc/AbsFunction.hpp"


namespace cie::fem::maths {


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
class BinaryOperator : public Result
{
public:
    CIE_DEFINE_CLASS_POINTERS(BinaryOperator)

public:
    BinaryOperator(typename LHS::SharedPointer&& rp_lhs,
                   typename RHS::SharedPointer&& rp_rhs);

    BinaryOperator(const typename LHS::SharedPointer& rp_lhs,
                   const typename RHS::SharedPointer& rp_rhs);

    BinaryOperator() = delete;

    BinaryOperator(BinaryOperator&& r_rhs) = default;

    BinaryOperator(const BinaryOperator& r_rhs) = default;

    BinaryOperator& operator=(BinaryOperator&& r_rhs) = default;

    BinaryOperator& operator=(const BinaryOperator& r_rhs) = default;

    const typename LHS::SharedPointer& lhs() const;

    const typename RHS::SharedPointer& rhs() const;

private:
    typename LHS::SharedPointer _p_lhs;
    typename RHS::SharedPointer _p_rhs;
}; // class BinaryOperator


} // namespace cie::fem::maths

#include "FEM/packages/maths/packages/operation/impl/BinaryOperator_impl.hpp"

#endif