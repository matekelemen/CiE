#ifndef CIE_FEM_MATHS_OPERATOR_PRODUCT_HPP
#define CIE_FEM_MATHS_OPERATOR_PRODUCT_HPP

// --- Internal Includes ---
#include "FEM/packages/maths/inc/MatrixFunction.hpp"
#include "FEM/packages/maths/inc/VectorFunction.hpp"
#include "FEM/packages/maths/inc/ScalarFunction.hpp"
#include "FEM/packages/maths/packages/operation/inc/BinaryOperator.hpp"


namespace cie::fem::maths {


template <concepts::AbsFunction LHS, concepts::AbsFunction RHS, concepts::AbsFunction Result>
class Product : public BinaryOperator<LHS,RHS,Result>
{
private:
    using Base = BinaryOperator<LHS,RHS,Result>;

public:
    using typename Base::value_type;
    using typename Base::argument_type;
    using typename Base::derivative_ptr;

    CIE_DEFINE_CLASS_POINTERS(Product)

public:
    using Base::Base;

    using Base::operator=;

    virtual value_type operator()(const argument_type& r_argument) const override;

    virtual derivative_ptr derivative() const override;
}; // class Product


namespace detail {

template <class LHS, class RHS>
struct ProductTraits {};

template <concepts::NotImplementedFunction LHS, concepts::ImplementedFunction RHS>
struct ProductTraits<LHS,RHS>
{using Result = NotImplementedFunction;};

template <concepts::ImplementedFunction LHS, concepts::NotImplementedFunction RHS>
struct ProductTraits<LHS,RHS>
{using Result = NotImplementedFunction;};

template <concepts::NotImplementedFunction LHS, concepts::NotImplementedFunction RHS>
struct ProductTraits<LHS,RHS>
{using Result = NotImplementedFunction;};

template <concepts::ScalarFunction Operand>
struct ProductTraits<Operand,Operand>
{
    using Result = ScalarFunction<Operand::argument_type::array_size,
                                  typename Operand::argument_type::value_type,
                                  typename Operand::derivative_type>;
};

template <concepts::ScalarFunction SF, concepts::VectorFunction VF>
struct ProductTraits<SF,VF>
{
    using Result = VectorFunction<VF::value_type::array_size,
                                  VF::argument_type::array_size,
                                  typename VF::value_type::value_type>;
};


template <concepts::ScalarFunction SF, concepts::VectorFunction VF>
struct ProductTraits<VF,SF>
{
    using Result = VectorFunction<VF::value_type::array_size,
                                  VF::argument_type::array_size,
                                  typename VF::value_type::value_type>;
};


// Special product: tensor product
template <concepts::VectorFunction VF>
struct ProductTraits<VF,VF>
{
    using Result = MatrixFunction<VF::value_type::array_size,
                                  VF::value_type::array_size,
                                  VF::argument_type::array_size,
                                  typename VF::value_type::value_type>;

    static typename Result::SharedPointer makeProduct(const typename VF::SharedPointer& rp_lhs,
                                                      const typename VF::SharedPointer& rp_rhs);
};


template <concepts::ScalarFunction SF, concepts::MatrixFunction MF>
struct ProductTraits<SF,MF>
{
    using Result = MatrixFunction<MF::value_type::RowTag,
                                  MF::value_type::ColumnTag,
                                  MF::argument_type::array_size,
                                  typename MF::value_type::value_type>;
};


template <concepts::ScalarFunction SF, concepts::MatrixFunction MF>
struct ProductTraits<MF,SF>
{
    using Result = MatrixFunction<MF::value_type::RowTag,
                                  MF::value_type::ColumnTag,
                                  MF::argument_type::array_size,
                                  typename MF::value_type::value_type>;
};


// Special product: vector^T * matrix
template <concepts::VectorFunction VF, concepts::MatrixFunction MF>
requires (VF::value_type::array_size == MF::value_type::RowTag)
struct ProductTraits<VF,MF>
{
    using Result = VectorFunction<MF::value_type::ColumnTag,
                                  VF::argument_type::array_size,
                                  typename VF::argument_type::value_type,
                                  NotImplementedFunction>;

    static typename Result::SharedPointer makeProduct(const typename VF::SharedPointer& rp_lhs,
                                                      const typename MF::SharedPointer& rp_rhs);
};


template <concepts::VectorFunction VF, concepts::MatrixFunction MF>
requires (VF::value_type::array_size == MF::value_type::ColumnTag)
struct ProductTraits<MF,VF>
{
    using Result = VectorFunction<MF::value_type::RowTag,
                                  VF::argument_type::array_size,
                                  typename VF::argument_type::value_type,
                                  NotImplementedFunction>;
};

template <concepts::MatrixFunction MF>
struct ProductTraits<MF,MF>
{
    using Result = MatrixFunction<MF::value_type::RowTag,
                                  MF::value_type::ColumnTag,
                                  MF::argument_type::array_size,
                                  typename MF::value_type::value_type>;
};


} // namespace detail
} // namespace cie::fem::maths


namespace cie::concepts::detail {
template <class LHS, class RHS>
concept HasSpecialProduct
=  AbsFunction<LHS>
&& AbsFunction<RHS>
&& requires (typename LHS::SharedPointer p_lhs, typename RHS::SharedPointer p_rhs)
{
    typename cie::fem::maths::detail::ProductTraits<LHS,RHS>::Result;
    {cie::fem::maths::detail::ProductTraits<LHS,RHS>::makeProduct(p_lhs,p_rhs)};
}; // concept HasSpecialProduct
} // namespace cie::concepts::detail


namespace cie::fem::maths {


template <concepts::AbsFunctionPtr LHSPtr, concepts::AbsFunctionPtr RHSPtr>
requires concepts::detail::HasSpecialProduct<typename std::decay<LHSPtr>::type::element_type,
                                             typename std::decay<RHSPtr>::type::element_type>
typename detail::ProductTraits<typename std::decay<LHSPtr>::type::element_type,
                               typename std::decay<RHSPtr>::type::element_type>::Result::SharedPointer
makeProduct(LHSPtr&& rp_lhs, RHSPtr&& rp_rhs);


template <concepts::AbsFunctionPtr LHSPtr, concepts::AbsFunctionPtr RHSPtr>
requires (!concepts::detail::HasSpecialProduct<typename std::decay<LHSPtr>::type::element_type,
                                               typename std::decay<RHSPtr>::type::element_type>)
typename detail::ProductTraits<typename std::decay<LHSPtr>::type::element_type,
                               typename std::decay<RHSPtr>::type::element_type>::Result::SharedPointer
makeProduct(LHSPtr&& rp_lhs, RHSPtr&& rp_rhs);


} // namespace cie::fem::maths

#include "FEM/packages/maths/packages/operation/impl/Product_impl.hpp"

#endif