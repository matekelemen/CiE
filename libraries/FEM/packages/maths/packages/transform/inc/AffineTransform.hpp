#ifndef CIE_FEM_MATHS_ABS_AFFINE_TRANSFORM_HPP
#define CIE_FEM_MATHS_ABS_AFFINE_TRANSFORM_HPP

// --- External Includes ---
#include <Eigen/Core>
#include <Eigen/LU>

// --- Utilty Includes ---
#include "cieutils/packages/compile_time/packages/concepts/inc/iterator_concepts.hpp"

// --- FEM Includes ---
#include "FEM/packages/maths/inc/VectorFunction.hpp"
#include "FEM/packages/utilities/inc/kernel.hpp"


namespace cie::fem::maths {


template <Size Dimension, concepts::Numeric NT>
class AffineTransform final : public VectorFunction<Dimension, Dimension, NT>
{
private:
    using Base = VectorFunction<Dimension,Dimension,NT>;

public:
    CIE_DEFINE_CLASS_POINTERS(AffineTransform)

    using CoefficientMatrix    = typename Kernel<Dimension,NT>::dense::static_matrix<(Dimension+1)*(Dimension+1), (Dimension+1)*(Dimension+1)>;
    using TransformationMatrix = typename Kernel<Dimension,NT>::dense::static_matrix<Dimension+1, Dimension+1>;
    using Factorizer           = Eigen::FullPivLU<typename CoefficientMatrix::Wrapped>;

    using typename Base::argument_type;
    using typename Base::value_type;
    using typename Base::derivative_ptr;

public:
    /// Identity transform by default
    AffineTransform();

    /** Affine transformation from (D+1) transformed points
     *
     *  The transformation is characterized by how it deforms a cube
     *  defined on [-1,1]^D. Not all corners of the deformed
     *  cube are necessary, but only the base ([-1]^D) and its adjecent
     *  ones (D+1 points in total). For example, the transformed location
     *  of the following vertices are required in 2D:
     *      [-1,-1], [1,-1], [-1,1]
     *
     *  To avoid unintended overlapping transformations, the order of vertices
     *  should match the following pattern (example in 3D):
     *      [-1,-1,-1], [1,-1,-1], [-1,1,-1], [-1,-1,1]
     *
     *  @param it_transformedBegin iterator pointing to the transformed cube's base ([-1]^D)
     *  @param it_transformedEnd iterator past the last transformed point (should be identical to it_transformedBegin + D + 1)
     */
    template <concepts::Iterator PointIterator>
    AffineTransform(PointIterator it_transformedBegin,
                    PointIterator it_transformedEnd);

    AffineTransform(TransformationMatrix&& r_transformationMatrix);

    AffineTransform(const TransformationMatrix& r_transformationMatrix);

    AffineTransform(AffineTransform&& r_rhs) = default;

    AffineTransform(const AffineTransform& r_rhs) = default;

    AffineTransform& operator=(AffineTransform&& r_rhs) = default;

    AffineTransform& operator=(const AffineTransform& r_rhs) = default;

    virtual value_type operator()(const argument_type& r_argument) const override;

    virtual derivative_ptr derivative() const override;

    const TransformationMatrix& transformationMatrix() const;

    AffineTransform inverse() const;

private:
    TransformationMatrix _transformationMatrix;

    // Factorized coefficient matrix for finding
    // transformation coefficients from transformed points
    static Factorizer _factorizer;
};


} // namespace cie::fem::maths

#include "FEM/packages/maths/packages/transform/impl/AffineTransform_impl.hpp"

#endif