#ifndef CIE_FEM_MATHS_ABS_AFFINE_TRANSFORM_IMPL_HPP
#define CIE_FEM_MATHS_ABS_AFFINE_TRANSFORM_IMPL_HPP

// --- Utility Includes ---
#include "cieutils/packages/stl_extension/inc/resize.hpp"


namespace cie::fem::maths {


template <Size Dimension, concepts::Numeric NT>
template <concepts::Iterator PointIterator>
AffineTransform<Dimension,NT>::AffineTransform(PointIterator it_transformedBegin,
                                               PointIterator it_transformedEnd)
{
    CIE_BEGIN_EXCEPTION_TRACING

    CIE_CHECK(
        std::distance(it_transformedBegin, it_transformedEnd) == Dimension + 1,
        "Invalid number of input points"
    )

    // Assemble RHS
    typename Kernel<Dimension,NT>::static_array<AffineTransform<Dimension,NT>::CoefficientMatrix::RowTag> rhs;

    Size index = 0;
    for ( ; it_transformedBegin!=it_transformedEnd; ++it_transformedBegin)
    {
        for (auto component : *it_transformedBegin)
            rhs[index++] = component;

        rhs[index++] = 1;
    }

    // Solve for transformation matrix components
    auto solution = _factorizer.solve(rhs.wrapped());

    // Copy components
    const Size dP1 = Dimension + 1;
    for (Size i_row=0; i_row<dP1; ++i_row)
    {
        for (Size i_column=0; i_column<dP1; ++i_column)
            _transformationMatrix(i_row, i_column) = solution[i_column + i_row*(dP1)];
    }

    CIE_END_EXCEPTION_TRACING
}


} // namespace cie::fem::maths


#endif