// --- FEM Includes ---
#include "FEM/packages/utilities/inc/template_macros.hpp"
#include "FEM/packages/maths/inc/ConstantMatrixFunction.hpp"

// --- Linalg Includes ---
#include "linalg/packages/overloads/inc/matrix_operators.hpp"

// --- Utility Includes ---
#include "cieutils/packages/stl_extension/inc/StaticArray.hpp"
#include "cieutils/packages/maths/inc/power.hpp"
#include "cieutils/packages/macros/inc/checks.hpp"

// --- Internal Includes ---
#include "FEM/packages/maths/packages/transform/inc/AffineTransform.hpp"

// --- STL Includes ---
#include <algorithm>


namespace cie::fem::maths {


namespace detail {


template <Size Dimension, concepts::Numeric NT>
typename AffineTransform<Dimension,NT>::CoefficientMatrix getCoefficientMatrix()
{
    CIE_BEGIN_EXCEPTION_TRACING

    typename AffineTransform<Dimension,NT>::CoefficientMatrix coefficientMatrix;
    coefficientMatrix.wrapped() = AffineTransform<Dimension,NT>::CoefficientMatrix::Wrapped::Zero();

    StaticArray<std::uint8_t,2> coordinates;
    coordinates[0] = -1;
    coordinates[1] = 1;

    const Size dP1 = Dimension + 1;
    const Size numberOfPoints = dP1;

    // Initial point
    StaticArray<std::int8_t, dP1> point;
    std::fill(point.begin(),
              point.end(),
              -1);

    // Augmented component
    point.back() = 1;

    for (Size i_point=0; i_point<numberOfPoints; ++i_point)
    {
        // Substitute point components into the coefficient matrix
        const Size rowOffset = i_point * dP1;

        for (Size i_component=0; i_component<dP1; ++i_component)
        {
            for (Size i_repeat=0; i_repeat<dP1; ++i_repeat)
            {
                const Size i_column = i_component + i_repeat*dP1;
                coefficientMatrix(rowOffset + i_repeat, i_column) = point[i_component];
            } // for i_repeat
        } // for i_component

        // Update point for the next iteration
        point[i_point] = 1;

        if (i_point != 0)
            point[i_point-1] = -1;
    } // for i_point

    return coefficientMatrix;

    CIE_END_EXCEPTION_TRACING
}


} // namespace detail


template <Size Dimension, concepts::Numeric NT>
typename AffineTransform<Dimension,NT>::Factorizer
AffineTransform<Dimension,NT>::_factorizer(detail::getCoefficientMatrix<Dimension,NT>().wrapped());


template <Size Dimension, concepts::Numeric NT>
AffineTransform<Dimension,NT>::AffineTransform()
    : _transformationMatrix(AffineTransform<Dimension,NT>::TransformationMatrix::makeIdentityMatrix())
{
}


template <Size Dimension, concepts::Numeric NT>
AffineTransform<Dimension,NT>::AffineTransform(typename AffineTransform<Dimension,NT>::TransformationMatrix&& r_transformationMatrix)
    : _transformationMatrix(std::move(r_transformationMatrix))
{
}


template <Size Dimension, concepts::Numeric NT>
AffineTransform<Dimension,NT>::AffineTransform(const typename AffineTransform<Dimension,NT>::TransformationMatrix& r_transformationMatrix)
    : _transformationMatrix(r_transformationMatrix)
{
}


template <Size Dimension, concepts::Numeric NT>
inline typename AffineTransform<Dimension,NT>::value_type
AffineTransform<Dimension,NT>::operator()(const typename AffineTransform<Dimension,NT>::argument_type& r_argument) const
{
    CIE_BEGIN_EXCEPTION_TRACING

    // Copy augmented point
    typename Kernel<Dimension,NT>::static_array<Dimension+1> augmentedPoint;
    std::copy(r_argument.begin(),
              r_argument.end(),
              augmentedPoint.begin());

    augmentedPoint[Dimension] = 1;

    // Transform
    auto transformed = _transformationMatrix * augmentedPoint;

    // Reduce
    typename AffineTransform<Dimension,NT>::value_type output;
    const NT scale = transformed[Dimension];

    CIE_DIVISION_BY_ZERO_CHECK(scale != 0)

    std::transform(
        transformed.begin(),
        transformed.begin() + Dimension,
        output.begin(),
        [scale](NT component) {return component / scale;}
    );

    return output;

    CIE_END_EXCEPTION_TRACING
}


template <Size Dimension, concepts::Numeric NT>
typename AffineTransform<Dimension,NT>::derivative_ptr
AffineTransform<Dimension,NT>::derivative() const
{
    CIE_BEGIN_EXCEPTION_TRACING

    typename AffineTransform<Dimension,NT>::derivative_type::value_type jacobian;
    CIE_OUT_OF_RANGE_CHECK(jacobian.rowSize() == Dimension)
    CIE_OUT_OF_RANGE_CHECK(jacobian.columnSize() == Dimension)

    NT scale = _transformationMatrix(Dimension, Dimension);
    CIE_DIVISION_BY_ZERO_CHECK(scale != 0)

    for (Size i_row=0; i_row<Dimension; ++i_row)
        for (Size i_column=0; i_column<Dimension; ++i_column)
            jacobian(i_row,i_column) = _transformationMatrix(i_row, i_column) / scale;

    return typename AffineTransform<Dimension,NT>::derivative_ptr(
        new ConstantMatrixFunction<Dimension,Dimension,Dimension,NT>(std::move(jacobian))
    );

    CIE_END_EXCEPTION_TRACING
}


template <Size Dimension, concepts::Numeric NT>
inline const typename AffineTransform<Dimension,NT>::TransformationMatrix&
AffineTransform<Dimension,NT>::transformationMatrix() const
{
    return _transformationMatrix;
}


template <Size Dimension, concepts::Numeric NT>
AffineTransform<Dimension,NT>
AffineTransform<Dimension,NT>::inverse() const
{
    CIE_BEGIN_EXCEPTION_TRACING

    return AffineTransform<Dimension,NT>(
        typename AffineTransform<Dimension,NT>::TransformationMatrix(_transformationMatrix.wrapped().inverse())
    );

    CIE_END_EXCEPTION_TRACING
}


CIE_FEM_INSTANTIATE_TEMPLATE(AffineTransform)


} // namespace cie::fem::maths