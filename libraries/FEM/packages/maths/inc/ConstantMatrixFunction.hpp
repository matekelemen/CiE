#ifndef CIE_FEM_MATHS_CONSTANT_MATRIX_FUNCTION_HPP
#define CIE_FEM_MATHS_CONSTANT_MATRIX_FUNCTION_HPP

// --- FEM Includes ---
#include "FEM/packages/maths/inc/MatrixFunction.hpp"


namespace cie::fem::maths {


template <Size ValueDimension0,
          Size ValueDimension1,
          Size Dimension,
          concepts::Numeric NT>
class ConstantMatrixFunction : public MatrixFunction<ValueDimension0,
                                                     ValueDimension1,
                                                     Dimension,
                                                     NT>
{
private:
    using Base = MatrixFunction<ValueDimension0,
                                ValueDimension1,
                                Dimension,
                                NT>;

public:
    CIE_DEFINE_CLASS_POINTERS(ConstantMatrixFunction)

    using typename Base::value_type;
    using typename Base::argument_type;
    using typename Base::derivative_ptr;

public:
    /// Identity by default
    ConstantMatrixFunction();

    ConstantMatrixFunction(value_type&& r_value);

    ConstantMatrixFunction(const value_type& r_value);

    ConstantMatrixFunction(ConstantMatrixFunction&& r_rhs) = default;

    ConstantMatrixFunction(const ConstantMatrixFunction& r_rhs) = default;

    ConstantMatrixFunction& operator=(ConstantMatrixFunction&& r_rhs) = default;

    ConstantMatrixFunction& operator=(const ConstantMatrixFunction& r_rhs) = default;

    virtual value_type operator()(const argument_type& r_argument) const override final;

    virtual derivative_ptr derivative() const override final;

protected:
    value_type _value;
};


} // namespace cie::fem::maths

// Implementation
#include "FEM/packages/maths/impl/ConstantMatrixFunction_impl.hpp"

#endif