#ifndef CIE_FEM_MATHS_MATRIX_FUNCTION_HPP
#define CIE_FEM_MATHS_MATRIX_FUNCTION_HPP

// --- Utility Includes ---
#include "cieutils/packages/compile_time/packages/concepts/inc/basic_concepts.hpp"
#include "cieutils/packages/types/inc/types.hpp"

// --- Internal Includes ---
#include "FEM/packages/maths/inc/NotImplementedFunction.hpp"
#include "FEM/packages/maths/inc/AbsFunction.hpp"
#include "FEM/packages/utilities/inc/kernel.hpp"

// --- STL Includes ---
#include <memory>


namespace cie::fem::maths {


template <Size ValueDimension0,
          Size ValueDimension1,
          Size Dimension,
          concepts::Numeric NT,
          class DerivativeType = NotImplementedFunction>
using MatrixFunction = AbsFunction<typename Kernel<Dimension,NT>::dense::static_matrix<ValueDimension0,ValueDimension1>,
                                   typename Kernel<Dimension,NT>::point_type,
                                   DerivativeType>;


template < Size VD0,
           Size VD1,
           Size Dimension,
           concepts::Numeric NT >
using MatrixFunctionPtr = std::shared_ptr<MatrixFunction<VD0,VD1,Dimension,NT>>;


} // namespace cie::fem::maths


namespace cie::concepts {

template <class T>
concept MatrixFunction
=  AbsFunction<T>
&& EigenMatrix<typename T::value_type>
&& !EigenVector<typename T::value_type>;

} // namespace cie::concepts


#endif