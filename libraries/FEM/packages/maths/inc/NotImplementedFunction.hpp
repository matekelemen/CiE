#ifndef CIE_FEM_MATHS_NOT_IMPLEMENTED_FUNCTION_HPP
#define CIE_FEM_MATHS_NOT_IMPLEMENTED_FUNCTION_HPP

// --- Internal Includes ---
#include "FEM/packages/maths/inc/AbsFunction.hpp"


namespace cie::fem::maths {


namespace detail {
class AllType
{
public:
    template <class ...Arguments>
    AllType( Arguments&&... r_arguments );

    template <class ...Arguments>
    AllType operator()( Arguments&&... r_arguments ) const;

    template <class ArgumentType>
    AllType operator[]( ArgumentType&& r_argument ) const;

    template <class T>
    operator T();

    template <class T>
    operator const T&() const;
}; // class AllType

AllType operator+(AllType lhs, AllType rhs);

AllType operator*(AllType lhs, AllType rhs);
} // namespace detail


class NotImplementedFunction : public AbsFunction<detail::AllType,detail::AllType,NotImplementedFunction>
{
private:
    using base_type = AbsFunction<detail::AllType,detail::AllType,NotImplementedFunction>;

public:
    CIE_DEFINE_CLASS_POINTERS( NotImplementedFunction )

public:
    base_type::value_type operator()( const base_type::argument_type& r_argument ) const override;

    base_type::derivative_ptr derivative() const override;
}; // class NotImplementedFunction


using NotImplementedFunctionPtr = NotImplementedFunction::SharedPointer;


} // namespace cie::fem::maths


namespace cie::concepts {

template <class T>
concept NotImplementedFunction
=  AbsFunction<T>
&& std::is_same_v<typename T::value_type, fem::maths::detail::AllType>
&& std::is_same_v<typename T::argument_type, fem::maths::detail::AllType>;

template <class T>
concept ImplementedFunction
= AbsFunction<T> && !NotImplementedFunction<T>;

} // namespace cie::concepts


#include "FEM/packages/maths/impl/NotImplementedFunction_impl.hpp"

#endif