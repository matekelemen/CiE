#ifndef CIE_FEM_MATHS_VECTOR_FUNCTION_HPP
#define CIE_FEM_MATHS_VECTOR_FUNCTION_HPP

// --- Utility Includes ---
#include "cieutils/packages/compile_time/packages/concepts/inc/container_concepts.hpp"
#include "cieutils/packages/types/inc/types.hpp"

// --- Internal Includes ---
#include "FEM/packages/maths/inc/MatrixFunction.hpp"
#include "FEM/packages/maths/inc/AbsFunction.hpp"
#include "FEM/packages/utilities/inc/kernel.hpp"


namespace cie::fem::maths {


template <Size ValueDimension,
          Size Dimension,
          concepts::Numeric NT,
          class DerivativeType = MatrixFunction<ValueDimension,Dimension,Dimension,NT>>
using VectorFunction = AbsFunction<typename Kernel<Dimension,NT>::static_array<ValueDimension>,
                                   typename Kernel<Dimension,NT>::point_type,
                                   DerivativeType>;


template <Size VD,
          Size D,
          concepts::Numeric NT>
using VectorFunctionPtr = std::shared_ptr<VectorFunction<VD,D,NT>>;


} // namespace cie::fem::maths


namespace cie::concepts {

template <class T>
concept VectorFunction
=  AbsFunction<T>
&& EigenVector<typename T::value_type>;

} // namespace cie::concepts


#endif