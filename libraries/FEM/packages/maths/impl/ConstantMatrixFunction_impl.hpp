#ifndef CIE_FEM_MATHS_CONSTANT_MATRIX_FUNCTION_IMPL_HPP
#define CIE_FEM_MATHS_CONSTANT_MATRIX_FUNCTION_IMPL_HPP


namespace cie::fem::maths {


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
ConstantMatrixFunction<VD0,VD1,D,NT>::ConstantMatrixFunction()
    : _value(ConstantMatrixFunction<VD0,VD1,D,NT>::value_type::makeIdentityMatrix())
{
}


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
ConstantMatrixFunction<VD0,VD1,D,NT>::ConstantMatrixFunction(typename ConstantMatrixFunction<VD0,VD1,D,NT>::value_type&& r_value)
    : _value(std::move(r_value))
{
}


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
ConstantMatrixFunction<VD0,VD1,D,NT>::ConstantMatrixFunction(const typename ConstantMatrixFunction<VD0,VD1,D,NT>::value_type& r_value)
    : _value(r_value)
{
}


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
inline typename ConstantMatrixFunction<VD0,VD1,D,NT>::value_type
ConstantMatrixFunction<VD0,VD1,D,NT>::operator()(const typename ConstantMatrixFunction<VD0,VD1,D,NT>::argument_type& r_argument) const
{
    return _value;
}


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
typename ConstantMatrixFunction<VD0,VD1,D,NT>::derivative_ptr
ConstantMatrixFunction<VD0,VD1,D,NT>::derivative() const
{
    return typename ConstantMatrixFunction<VD0,VD1,D,NT>::derivative_ptr(new NotImplementedFunction);
}


} // namespace cie::fem::maths


#endif