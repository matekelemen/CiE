#ifndef CIE_FEM_MATHS_SEPARABLE_SCALAR_FUNCTION_IMPL_HPP
#define CIE_FEM_MATHS_SEPARABLE_SCALAR_FUNCTION_IMPL_HPP

// --- Utility Includes ---
#include "cieutils/packages/macros/inc/exceptions.hpp"


namespace cie::fem::maths {


template <Size Dimension, concepts::Numeric NT>
inline typename SeparableScalarFunction<Dimension,NT>::value_type
SeparableScalarFunction<Dimension,NT>::operator()(const typename SeparableScalarFunction<Dimension,NT>::argument_type& r_argument) const
{
    CIE_BEGIN_EXCEPTION_TRACING

    typename SeparableScalarFunction<Dimension,NT>::value_type value = 1;

    auto it_argument = r_argument.begin();

    for (const auto& rp_univariate : this->_univariates)
        value *= rp_univariate->operator()({*it_argument++});

    return value;

    CIE_END_EXCEPTION_TRACING
}


} // namespace cie::fem::maths


#endif