#ifndef CIE_FEM_MATHS_SEPARABLE_MATRIX_FUNCTION_IMPL_HPP
#define CIE_FEM_MATHS_SEPARABLE_MATRIX_FUNCTION_IMPL_HPP

// --- Utility Includes ---
#include "cieutils/packages/macros/inc/exceptions.hpp"
#include "cieutils/packages/stl_extension/inc/resize.hpp"
#include "cieutils/packages/macros/inc/checks.hpp"

// --- STL Includes ---
#include <algorithm>


namespace cie::fem::maths {


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
SeparableMatrixFunction<VD0,VD1,D,NT>::SeparableMatrixFunction( typename SeparableMatrixFunction<VD0,VD1,D,NT>::function_container&& r_functions ) :
    _functions( std::move(r_functions) )
{
}


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
SeparableMatrixFunction<VD0,VD1,D,NT>::SeparableMatrixFunction( const typename SeparableMatrixFunction<VD0,VD1,D,NT>::function_container& r_functions ) :
    _functions( r_functions )
{
}


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
SeparableMatrixFunction<VD0,VD1,D,NT>::SeparableMatrixFunction( typename SeparableMatrixFunction<VD0,VD1,D,NT>::function_list&& r_functions )
{
    CIE_BEGIN_EXCEPTION_TRACING

    CIE_OUT_OF_RANGE_CHECK( r_functions.size() == VD0 )
    utils::resize( this->_functions, VD0 );

    std::copy( r_functions.begin(),
               r_functions.end(),
               this->_functions.begin() );

    CIE_END_EXCEPTION_TRACING
}


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
inline typename SeparableMatrixFunction<VD0,VD1,D,NT>::value_type
SeparableMatrixFunction<VD0,VD1,D,NT>::operator()( const typename SeparableMatrixFunction<VD0,VD1,D,NT>::argument_type& r_argument ) const
{
    CIE_BEGIN_EXCEPTION_TRACING

    typename SeparableMatrixFunction<VD0,VD1,D,NT>::value_type value;

    for ( Size functionIndex=0; functionIndex<_functions.size(); ++functionIndex )
    {
        auto vector = _functions[functionIndex]->evaluate( r_argument );
        for ( Size componentIndex=0; componentIndex<value.columnSize(); ++componentIndex )
            value( functionIndex, componentIndex ) = vector[componentIndex];
    }

    return value;

    CIE_END_EXCEPTION_TRACING
}


template <Size VD0, Size VD1, Size D, concepts::Numeric NT>
typename SeparableMatrixFunction<VD0,VD1,D,NT>::derivative_ptr
SeparableMatrixFunction<VD0,VD1,D,NT>::derivative() const
{
    return typename SeparableMatrixFunction<VD0,VD1,D,NT>::derivative_ptr(
        new NotImplementedFunction
    );
}


} // namespace cie::fem::maths


#endif