#ifndef CIE_UTILS_COMPILE_TIME_PARAMETER_PACK_MATCH_HPP
#define CIE_UTILS_COMPILE_TIME_PARAMETER_PACK_MATCH_HPP

// --- Utility Includes ---
#include "cieutils/packages/types/inc/types.hpp"
#include "cieutils/packages/compile_time/packages/parameter_pack/inc/Select.hpp"
#include "cieutils/packages/compile_time/packages/parameter_pack/inc/Size.hpp"

// --- STL Includes ---
#include <type_traits>
#include <tuple>


namespace cie::ct {


namespace detail {

/// Compare a type to a parameter pack
template <class T>
class MatchOne
{
private:
    template <Size I, class ...TRefs>
    requires (!Empty<TRefs...>)
    static constexpr bool any()
    {
        if constexpr (std::is_same_v<T,typename Select<TRefs...>::template At<I>>)
            return true;

        if constexpr (I == 0)
            return false;
        else
            return any<I-1,TRefs...>();
    }

    template <Size I, class ...TRefs>
    requires Empty<TRefs...>
    static constexpr bool any() {return false;}

public:
    /// T is in @ref{TRefs}
    template <class ...TRefs>
    inline static constexpr bool Any = any<PackSize<TRefs...>-1,TRefs...>();

    /// T is not in @ref{TRefs}
    template <class ...TRefs>
    inline static constexpr bool None = !Any<TRefs...>;
}; // class MatchOne

} // namespace detail


/// Compare two parameter packs
template <class ...Ts>
class Match
{
private:
    template <Size I, class ...TRefs>
    requires (!Empty<TRefs...>)
    static constexpr bool any()
    {
        using Current = typename Select<Ts...>::template At<I>;
        if constexpr (detail::MatchOne<Current>::template Any<TRefs...>)
            return true;

        if constexpr (I == 0)
            return false;
        else
            return any<I-1,TRefs...>();
    }

    template <Size I, class ...TRefs>
    requires Empty<TRefs...>
    static constexpr bool any() {return false;}

public:
    /// Any of @ref{Ts} is in TRefs
    template <class ...TRefs>
    inline static constexpr bool Any = any<PackSize<Ts...>-1,TRefs...>();

    /// None of @ref{Ts} is in TRefs
    template <class ...TRefs>
    inline static constexpr bool None = !Any<TRefs...>;
}; // class Match


/// Compare the parameter packs of two tuples
template <class T>
class MatchTuple {};


/// Compare the parameter packs of two tuples
template <class ...Ts>
class MatchTuple<std::tuple<Ts...>>
{
private:
    template <class T>
    struct AnyImpl {};

    template <class ...TTs>
    struct AnyImpl<std::tuple<TTs...>>
    {inline static constexpr bool Value = Match<Ts...>::template Any<TTs...>;};

public:
    /// True if any type in the tuple is also in the input tuple
    template <class TTuple>
    inline static constexpr bool Any = AnyImpl<TTuple>::Value;

    /// True if none of the tuple's types are in the input tuple
    template <class TTuple>
    inline static constexpr bool None = !Any<TTuple>;
}; // class MatchTuple


} // namespace cie::ct


#endif