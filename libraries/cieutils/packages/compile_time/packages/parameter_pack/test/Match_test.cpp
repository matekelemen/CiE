// --- Utility Includes ---
#include "cieutils/packages/testing/inc/essentials.hpp"
#include "cieutils/packages/compile_time/packages/parameter_pack/inc/Match.hpp"


namespace cie::ct {


CIE_TEST_CASE("Match", "[compile_time]")
{

    CIE_TEST_CASE_INIT("Match")

    {
        CIE_TEST_CASE_INIT("Any")

        CIE_TEST_CHECK(Match<int>::Any<char,void,double,int>);
        CIE_TEST_CHECK(Match<int>::Any<int>);
        CIE_TEST_CHECK(!Match<int>::Any<void,float,double>);
        CIE_TEST_CHECK(!Match<int>::Any<unsigned int>);
        CIE_TEST_CHECK(!Match<int>::Any<>);

        CIE_TEST_CHECK(Match<int,char>::Any<void,double,int>);
        CIE_TEST_CHECK(Match<int,char>::Any<int>);
        CIE_TEST_CHECK(!Match<int,char>::Any<void,float,double>);
        CIE_TEST_CHECK(!Match<int,char>::Any<unsigned int>);
        CIE_TEST_CHECK(!Match<int,char>::Any<>);

        CIE_TEST_CHECK(!Match<>::Any<>);
    }

    {
        CIE_TEST_CASE_INIT("None")

        CIE_TEST_CHECK(!Match<int>::None<char,void,double,int>);
        CIE_TEST_CHECK(!Match<int>::None<int>);
        CIE_TEST_CHECK(Match<int>::None<void,float,double>);
        CIE_TEST_CHECK(Match<int>::None<unsigned int>);
        CIE_TEST_CHECK(Match<int>::None<>);

        CIE_TEST_CHECK(!Match<int,char>::None<void,double,int>);
        CIE_TEST_CHECK(!Match<int,char>::None<int>);
        CIE_TEST_CHECK(Match<int,char>::None<void,float,double>);
        CIE_TEST_CHECK(Match<int,char>::None<unsigned int>);
        CIE_TEST_CHECK(Match<int,char>::None<>);

        CIE_TEST_CHECK(Match<>::None<>);
    }
}


CIE_TEST_CASE("MatchTuple", "[compile_time]")
{

    CIE_TEST_CASE_INIT("MatchTuple")

    {
        CIE_TEST_CASE_INIT("Any")

        CIE_TEST_CHECK(MatchTuple<std::tuple<int>>::Any<std::tuple<char,void,double,int>>);
        CIE_TEST_CHECK(MatchTuple<std::tuple<int>>::Any<std::tuple<int>>);
        CIE_TEST_CHECK(!MatchTuple<std::tuple<int>>::Any<std::tuple<void,float,double>>);
        CIE_TEST_CHECK(!MatchTuple<std::tuple<int>>::Any<std::tuple<unsigned int>>);
        CIE_TEST_CHECK(!MatchTuple<std::tuple<int>>::Any<std::tuple<>>);

        CIE_TEST_CHECK(MatchTuple<std::tuple<int,char>>::Any<std::tuple<void,double,int>>);
        CIE_TEST_CHECK(MatchTuple<std::tuple<int,char>>::Any<std::tuple<int>>);
        CIE_TEST_CHECK(!MatchTuple<std::tuple<int,char>>::Any<std::tuple<void,float,double>>);
        CIE_TEST_CHECK(!MatchTuple<std::tuple<int,char>>::Any<std::tuple<unsigned int>>);
        CIE_TEST_CHECK(!MatchTuple<std::tuple<int,char>>::Any<std::tuple<>>);

        CIE_TEST_CHECK(!MatchTuple<std::tuple<>>::Any<std::tuple<>>);
    }

    {
        CIE_TEST_CASE_INIT("None")

        CIE_TEST_CHECK(!MatchTuple<std::tuple<int>>::None<std::tuple<char,void,double,int>>);
        CIE_TEST_CHECK(!MatchTuple<std::tuple<int>>::None<std::tuple<int>>);
        CIE_TEST_CHECK(MatchTuple<std::tuple<int>>::None<std::tuple<void,float,double>>);
        CIE_TEST_CHECK(MatchTuple<std::tuple<int>>::None<std::tuple<unsigned int>>);
        CIE_TEST_CHECK(MatchTuple<std::tuple<int>>::None<std::tuple<>>);

        CIE_TEST_CHECK(!MatchTuple<std::tuple<int,char>>::None<std::tuple<void,double,int>>);
        CIE_TEST_CHECK(!MatchTuple<std::tuple<int,char>>::None<std::tuple<int>>);
        CIE_TEST_CHECK(MatchTuple<std::tuple<int,char>>::None<std::tuple<void,float,double>>);
        CIE_TEST_CHECK(MatchTuple<std::tuple<int,char>>::None<std::tuple<unsigned int>>);
        CIE_TEST_CHECK(MatchTuple<std::tuple<int,char>>::None<std::tuple<>>);

        CIE_TEST_CHECK(MatchTuple<std::tuple<>>::None<std::tuple<>>);
    }
}


} // namespace cie::ct