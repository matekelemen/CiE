// --- Utility Includes ---
#include "cieutils/packages/testing/inc/essentials.hpp"
#include "cieutils/packages/compile_time/packages/parameter_pack/inc/Range.hpp"


namespace cie::ct {


template <class ...Ts>
struct RangeTest {};


CIE_TEST_CASE("Range", "[compile_time]")
{

    CIE_TEST_CASE_INIT("Range")

    using Full = RangeTest<void,char,char,int,double,RangeTest<>>;
    using RangeBase = Range<void,char,char,int,double,RangeTest<>>;

    using Reference_Begin_3 = RangeTest<void,char,char>;
    using Reference_3_End = RangeTest<int,double,RangeTest<>>;
    using Reference_1_3 = RangeTest<char,char>;
    using Reference_First = RangeTest<void>;
    using Reference_Last = RangeTest<RangeTest<>>;
    using Reference_Full = Full;
    using Reference_Empty = RangeTest<>;

    CIE_TEST_CHECK(std::is_same_v<RangeBase::Subrange<0,3>::Apply<RangeTest>, Reference_Begin_3>);
    CIE_TEST_CHECK(std::is_same_v<RangeBase::Subrange<3,6>::Apply<RangeTest>, Reference_3_End>);
    CIE_TEST_CHECK(std::is_same_v<RangeBase::Subrange<1,3>::Apply<RangeTest>, Reference_1_3>);
    CIE_TEST_CHECK(std::is_same_v<RangeBase::Subrange<0,1>::Apply<RangeTest>, Reference_First>);
    CIE_TEST_CHECK(std::is_same_v<RangeBase::Subrange<5,6>::Apply<RangeTest>, Reference_Last>);
    CIE_TEST_CHECK(std::is_same_v<RangeBase::Subrange<0,6>::Apply<RangeTest>, Reference_Full>);
    CIE_TEST_CHECK(std::is_same_v<RangeBase::Subrange<0,0>::Apply<RangeTest>, Reference_Empty>);
    CIE_TEST_CHECK(std::is_same_v<RangeBase::Subrange<3,3>::Apply<RangeTest>, Reference_Empty>);
    CIE_TEST_CHECK(std::is_same_v<RangeBase::Subrange<5,5>::Apply<RangeTest>, Reference_Empty>);
}


} // namespace cie::ct