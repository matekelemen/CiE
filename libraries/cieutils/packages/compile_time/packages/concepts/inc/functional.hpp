#ifndef CIE_UTILS_CONCEPTS_FUNCTIONAL_HPP
#define CIE_UTILS_CONCEPTS_FUNCTIONAL_HPP

// --- Utility Includes ---
#include "cieutils/packages/stl_extension/inc/FunctionTraits.hpp"

// --- STL Includes ---
#include <concepts>
#include <functional>
#include <type_traits>


namespace cie::concepts {


template <class T>
concept STLFunction
= FunctionTraits<T>::value;


template <class T>
concept RawFunction
= std::is_function<T>::value;


template <class T>
concept Function
= STLFunction<T> || RawFunction<T>;


template <class F, class A>
concept FunctionWithArgument
=  STLFunction<F>
&& FunctionTraits<F>::NumberOfArguments == 1
&& std::is_same_v<A, typename FunctionTraits<F>::template Argument<0>::type>;

namespace detail {
template <STLFunction TFunction, class ...TArguments>
struct IsFunctionWithArguments
{
    template <Size I, bool All>
    struct IsFunctionWithArgumentAtRecursive
    {
        static constexpr bool Value = All && std::is_same_v<
            typename std::tuple_element<I,std::tuple<TArguments...>>::type,
            typename std::tuple_element<I,typename FunctionTraits<TFunction>::ArgumentTuple>::type
        >;

        static constexpr bool value() {return IsFunctionWithArgumentAtRecursive<I-1,Value>::value();}
    }; // struct IsFunctionWithArgumentAtRecursive

    template <bool All>
    struct IsFunctionWithArgumentAtRecursive<0,All>
    {
        static constexpr bool Value = All && std::is_same_v<
            typename std::tuple_element<0,std::tuple<TArguments...>>::type,
            typename std::tuple_element<0,typename FunctionTraits<TFunction>::ArgumentTuple>::type
        >;

        static constexpr bool value() {return Value;}
    }; // struct isFunctionWithArgumentAtRecursive specialized for 0

    using Traits = FunctionTraits<TFunction>;

    static constexpr bool Value = IsFunctionWithArgumentAtRecursive<Traits::NumberOfArguments-1,true>::value();
}; // struct IsFunctionWithArguments
} // namespace detail


template <class TFunction, class ...TArguments>
concept FunctionWithArguments
=  STLFunction<TFunction>
&& FunctionTraits<TFunction>::NumberOfArguments == sizeof...(TArguments)
&& detail::IsFunctionWithArguments<TFunction,TArguments...>::Value;


} // namespace cie::concepts


#endif