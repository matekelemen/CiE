#ifndef CIE_CIEUTILS_BASIC_CONCEPTS_HPP
#define CIE_CIEUTILS_BASIC_CONCEPTS_HPP

// --- STL Includes ---
#include <concepts>
#include <type_traits>
#include <cstdint>
#include <utility>


namespace cie::concepts {


// ---------------------------------------------------------
// STL Extension
// ---------------------------------------------------------

template <class T, class TT>
concept same_as_noqualifiers
= std::is_same_v<T,TT> || std::is_same_v<typename std::remove_const<T>::type,TT>;

template <class T>
concept Const
= std::is_const_v<typename std::remove_reference<T>::type>;


template <class T>
concept NonConst
= !Const<T>;

template <class T, class ...TT>
concept AnyOf
= (std::is_same_v<T,TT> || ...);

template <class T, class ...TT>
concept NoneOf
= !AnyOf<T,TT...>;

// ---------------------------------------------------------
// Life Cycle
// ---------------------------------------------------------

template <class T>
concept Constructible
= std::is_constructible_v<T>;

template <class T>
concept DefaultConstructible
= std::is_default_constructible_v<T>;

template <class T>
concept MoveConstructible
= std::is_move_constructible_v<T>;

template <class T>
concept CopyConstructible
= std::is_copy_constructible_v<T>;

template <class T>
concept MoveAssignable
= std::is_move_assignable_v<T>;

template <class T>
concept CopyAssignable
= std::is_copy_assignable_v<T>;

template <class T>
concept Destructible
= std::is_destructible_v<T>;

template <class T, class ...Arguments>
concept ConstructibleFrom
= requires(Arguments&&... r_arguments)
{
    {T(std::forward<Arguments>(r_arguments)...)};
};


// ---------------------------------------------------------
// Polymorphism
// ---------------------------------------------------------

namespace detail {
template<typename T>
struct void_ {typedef void type;};

template<typename T, typename = void>
struct CanBeDerivedFrom {
  static const bool value = false;
};

template<typename T>
struct CanBeDerivedFrom<T, typename void_<int T::*>::type> {
  static const bool value = true;
};
} // namespace detail


template <class T>
concept Deriveable
= detail::CanBeDerivedFrom<T>::value
    && !std::is_integral_v<T>
    && !std::is_floating_point_v<T>;


template <class T>
concept NonDeriveable
= !Deriveable<T>;

template <class DerivedType, class BaseType>
concept DerivedFrom
= std::derived_from<DerivedType,BaseType>;


// ---------------------------------------------------------
// Numerics
// ---------------------------------------------------------

template <class T>
concept Integral
= std::is_integral_v<T>;

template <class T>
concept Incrementable
= requires (T instance)
{
    {++instance};
    {instance++};
};


template <class T>
concept Decrementable
= requires (T instance)
{
    {--instance};
    {instance--};
};


template <class T>
concept Addable
= requires (T instance)
{
    {instance + instance}     -> std::same_as<T>;
    {instance += instance}    -> std::same_as<T&>;
};


template <class T>
concept Subtractable
= requires (T instance)
{
    {instance - instance}     -> std::same_as<T>;
    {instance -= instance}    -> std::same_as<T&>;
};


template <class T>
concept Multipliable
= requires (T instance)
{
    {instance * instance}     -> std::same_as<T>;
    {instance *= instance}    -> std::same_as<T&>;
};


template <class T>
concept Divisible
= requires (T instance)
{
    {instance / instance}     -> std::same_as<T>;
    {instance /= instance}    -> std::same_as<T&>;
};


template <class T>
concept Numeric
=  Incrementable<T>
&& Decrementable<T>
&& Addable<T>
&& Subtractable<T>
&& Multipliable<T>
&& Divisible<T>;


template <class T>
concept UnsignedInteger
= AnyOf<T, uint8_t,
           uint16_t,
           uint32_t,
           uint64_t,
           uint_fast8_t,
           uint_fast16_t,
           uint_fast32_t,
           uint_fast64_t,
           uint_least8_t,
           uint_least16_t,
           uint_least32_t,
           uint_least64_t>;

template <class T>
concept SignedInteger
= AnyOf<T, int8_t,
           int16_t,
           int32_t,
           int64_t,
           int_fast8_t,
           int_fast16_t,
           int_fast32_t,
           int_fast64_t,
           int_least8_t,
           int_least16_t,
           int_least32_t,
           int_least64_t>;

template <class T>
concept Integer
= UnsignedInteger<T> || SignedInteger<T>;


// ---------------------------------------------------------
// Casting and Conversion
// ---------------------------------------------------------

template <class SourceType, class TargetType>
concept ConvertibleTo
= requires (SourceType instance)
{
    {TargetType(instance)};
};


// ---------------------------------------------------------
// Pointers
// ---------------------------------------------------------

template <class T>
concept Dereferencable
= requires (T instance)
{
    {*instance};
    {instance.operator->()};
};


template <class T>
concept RawPointer
= std::is_pointer_v<T>;


template <class T>
concept NonRawPointer
= !std::is_pointer_v<T>;


template <class T>
concept Pointer
= RawPointer<T>
  || Dereferencable<T>;


template <class T>
concept NonPointer
= !Pointer<T>;


template <class PointerType, class ValueType>
concept ClassPointer
=  Pointer<PointerType>
&& requires (PointerType instance)
{
    {*instance} -> std::same_as<ValueType&>;
};


template <class PointerType, class ValueType>
concept ClassRawPointer
=  RawPointer<PointerType>
&& ClassPointer<PointerType, ValueType>;


// ---------------------------------------------------------
// MISC
// ---------------------------------------------------------

namespace detail {
template <class T>
struct FalseType : std::false_type {};
} // namespace detail


template <class T>
concept Unsatisfiable
= detail::FalseType<T>::value;

} // namespace cie::concepts

#endif