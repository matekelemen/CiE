#ifndef CIE_CIEUTILS_CONTAINER_CONCEPTS_HPP
#define CIE_CIEUTILS_CONTAINER_CONCEPTS_HPP

// --- Internal Includes ---
#include "cieutils/packages/compile_time/packages/concepts/inc/iterator_concepts.hpp"


namespace cie::concepts {


namespace detail {

// Member requirements
template < class T,
           class ArgumentType = typename T::size_type,
           class ReturnType = typename T::reference >
concept HasAt
= requires ( T instance, ArgumentType argument )
{
    { instance.at(argument) } -> std::same_as<ReturnType>;
};


template < class T,
           class ArgumentType = typename T::size_type,
           class ReturnType = typename T::reference >
concept HasAccessOperator
= requires ( T instance, ArgumentType argument )
{
    { instance[argument] } -> std::same_as<ReturnType>;
};


template < class T,
           class ReturnType = typename T::reference >
concept HasFront
= requires ( T instance )
{
    { instance.front() } -> std::same_as<ReturnType>;
};


template < class T,
           class ReturnType = typename T::reference >
concept HasBack
= requires ( T instance )
{
    { instance.back() } -> std::same_as<ReturnType>;
};


template < class T,
           class ...Arguments>
concept HasResize
= requires ( T instance, Arguments... arguments )
{
    { instance.resize(arguments...) } -> std::same_as<void>;
};


template < class T,
           class ArgumentType = typename T::size_type,
           class ReturnType = void >
concept HasReserve
= requires ( T instance, ArgumentType argument )
{
    { instance.reserve(argument) } -> std::same_as<ReturnType>;
};


template <class T>
concept HasClear
= requires ( T instance )
{
    { instance.clear() };
};


template < class T,
           class ReturnType = typename T::iterator >
concept HasErase
= requires ( T instance )
{
    { instance.erase(instance.begin(), instance.end()) } -> std::same_as<ReturnType>;
};


template < class T,
           class ArgumentType = const typename T::value_type&,
           class ReturnType = void >
concept HasPushFront
= requires ( T instance, ArgumentType argument )
{
    { instance.push_front(argument) } -> std::same_as<ReturnType>;
};


template < class T,
           class ArgumentType = const typename T::value_type&,
           class ReturnType = void >
concept HasPushBack
= requires ( T instance, ArgumentType argument )
{
    { instance.push_back(argument) } -> std::same_as<ReturnType>;
};


template < class T,
           class ...Arguments >
concept HasEmplaceFront
= requires ( T instance, Arguments&&... r_arguments )
{
    { instance.emplace_front(std::forward<Arguments>(r_arguments)...) } -> std::same_as<typename T::reference>;
};


template < class T,
           class ...Arguments >
concept HasEmplaceBack
= requires ( T instance, Arguments&&... r_arguments )
{
    { instance.emplace_back(std::forward<Arguments>(r_arguments)...) } -> std::same_as<typename T::reference>;
};


template < class T,
           class ReturnType = void >
concept HasPopFront
= requires ( T instance)
{
    { instance.pop_back() } -> std::same_as<ReturnType>;
};


template < class T,
           class ReturnType = void >
concept HasPopBack
= requires ( T instance)
{
    { instance.pop_back() } -> std::same_as<ReturnType>;
};


template < class T,
           class ReturnType = void >
concept HasSwap
= requires ( T instance, T swap)
{
    { instance.swap(swap) } -> std::same_as<ReturnType>;
};


} // namespace detail


template <class T>
concept STLContainer
= requires (T instance, const T constInstance)
{
    typename T::value_type;
    typename T::size_type;
    typename T::iterator;
    typename T::const_iterator;
    {instance.size()};
    {instance.begin()};
    {instance.end()};
    {constInstance.begin()};
    {constInstance.end()};
};


template <class ContainerType, class ValueType>
concept ClassContainer
= STLContainer<typename std::remove_reference<ContainerType>::type>
  && std::is_same_v<typename std::remove_reference<ContainerType>::type::value_type, ValueType>;


template <class ContainerType>
concept NumericContainer
= STLContainer<typename std::decay<ContainerType>::type>
  && Numeric<typename std::decay<ContainerType>::type::value_type>;


template <class ContainerType>
concept PointerContainer
= STLContainer<typename std::remove_reference<ContainerType>::type>
  && Pointer<typename std::remove_reference<ContainerType>::type::value_type>;


template <class ContainerType>
concept NonPointerContainer
= STLContainer<typename std::remove_reference<ContainerType>::type>
  && !PointerContainer<ContainerType>;


template <class ContainerType>
concept IteratorContainer
= STLContainer<typename std::remove_reference<ContainerType>::type>
  && Iterator<typename std::remove_reference<ContainerType>::type::value_type>;


template <class ContainerType, class ValueType>
concept ClassPointerContainer
= STLContainer<typename std::remove_reference<ContainerType>::type>
  && ClassPointer<typename std::remove_reference<ContainerType>::type::value_type,ValueType>;


template <class ContainerType, class InterfaceType>
concept InterfaceContainer
= STLContainer<typename std::remove_reference<ContainerType>::type>
  && Pointer<typename std::remove_reference<ContainerType>::type::value_type>
  && DerivedFrom<typename std::pointer_traits<typename std::remove_reference<ContainerType>::type::value_type>::element_type,InterfaceType>;


// ---------------------------------------------------------
// SPECIALIZED STL CONTAINERS
// ---------------------------------------------------------

template <class ContainerType>
concept ResizableContainer
= STLContainer<ContainerType>
  && requires ( ContainerType instance )
{
    { instance.resize(1) };
};


template <class ContainerType>
concept ReservableContainer
= STLContainer<ContainerType>
  && requires ( ContainerType instance )
{
    { instance.reserve(1) };
};


// TODO: improve definition
template <class ContainerType>
concept FixedSizeContainer
=   STLContainer<ContainerType>
    && !ResizableContainer<ContainerType>;


template <class ContainerType>
concept STLArray
= FixedSizeContainer<ContainerType>;


} // namespace cie::concepts

#endif