// --- Utility Includes ---
#include "cieutils/packages/macros/inc/exceptions.hpp"
#include "cieutils/packages/testing/inc/essentials.hpp"
#include "cieutils/packages/commandline/inc/ArgParse.hpp"
#include "cieutils/packages/stl_extension/inc/zip.hpp"


namespace cie::utils {


CIE_TEST_CASE("ArgParse", "[commandline]")
{
    CIE_TEST_CASE_INIT("ArgParse")

    CIE_TEST_CHECK_NOTHROW(ArgParse());
    ArgParse parser;

    CIE_TEST_CHECK_NOTHROW(parser.addPositional("pos"));
    CIE_TEST_CHECK_NOTHROW(parser.addKeyword({"-s", "--start"}));
    CIE_TEST_CHECK_NOTHROW(parser.addFlag({"-k"}));
    CIE_TEST_CHECK_NOTHROW(parser.addFlag({"-r"}));

    const int argc = 4;
    char const* argv[argc] = {
        "a_positional_argument",
        "--start",
        "a_keyword_argument",
        "-k"
    };

    ArgParse::Results results;
    CIE_TEST_CHECK_NOTHROW(results = parser.parseArguments(argc,argv));

    const ArgParse::Results referenceResults {
        {"pos", {"a_positional_argument"}},
        {"-s", {"a_keyword_argument"}},
        {"-k", {"true"}},
        {"-r", {"false"}}
    };

    for (const auto& r_pair : referenceResults)
    {
        auto it = results.find(r_pair.first);
        CIE_TEST_CHECK(it != results.end());
        CIE_TEST_CHECK(it->second.size() == r_pair.second.size());

        for (const auto& [r_value, r_reference] : utils::zip(it->second, r_pair.second))
            CIE_TEST_CHECK(r_value == r_reference);
    }
}


} // namespace cie::utils
