#ifndef CIE_UTILS_COMMAND_LINE_KEYWORD_PARSER_HPP
#define CIE_UTILS_COMMAND_LINE_KEYWORD_PARSER_HPP

// --- Utility Includes ---
#include "cieutils/packages/commandline/inc/ArgParse.hpp"

// --- STL Includes ---
#include <regex>


namespace cie::utils::detail {


struct KeywordParser
{
public:
    static ArgParse::Key parse(const ArgParse::Key& r_key);

    static const std::regex regex;
};


} // namespace cie::utils::detail


#endif