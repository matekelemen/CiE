#ifndef CIE_UTILS_ARG_PARSE_HPP
#define CIE_UTILS_ARG_PARSE_HPP

// --- Utility Includes ---
#include "cieutils/packages/stl_extension/inc/DynamicArray.hpp"
#include "cieutils/packages/macros/inc/typedefs.hpp"

// --- STL Includes ---
#include <functional>
#include <ranges>
#include <string_view>
#include <unordered_map>


namespace cie::utils {


class ArgParse
{
public:
    CIE_DEFINE_CLASS_POINTERS(ArgParse)

    using Key = std::string;

    using KeyView = std::basic_string_view<Key::value_type>;

    using KeyContainer = DynamicArray<Key>;

    using Value = std::string;

    using ValueContainer = DynamicArray<Value>;

    using ValueView = std::ranges::subrange<const Value::value_type*>;

    using ValueViewContainer = DynamicArray<ValueView>;

    using DefaultValue = ValueContainer;

    using Validator = std::function<bool(const ValueView&)>;

    using Results = std::unordered_map<std::string,ValueContainer>;

    class ArgumentCount
    {
    public:
        static const ArgumentCount None;

        static const ArgumentCount One;

        static const ArgumentCount NonZero;

        static const ArgumentCount Any;

        static const ArgumentCount exactly(Size count);

    public:
        constexpr ArgumentCount()
            : ArgumentCount(1)
        {}

        constexpr ArgumentCount(int value)
            : _value(value)
        {}

        constexpr ArgumentCount(ArgumentCount&& r_rhs) = default;

        constexpr ArgumentCount(const ArgumentCount& r_rhs) = default;

        constexpr ArgumentCount& operator=(ArgumentCount&& r_rhs) = default;

        constexpr ArgumentCount& operator=(const ArgumentCount& r_rhs) = default;

        friend bool operator==(const ArgumentCount lhs, const ArgumentCount rhs)
        {return lhs._value == rhs._value;}

        friend bool operator!=(const ArgumentCount lhs, const ArgumentCount rhs)
        {return lhs._value != rhs._value;}

        explicit operator int () const
        {return _value;}

    private:
        int _value;
    }; // class ArgumentCount

    static const Validator defaultValidator;

public:
    /// Default constructor initializing pimpl.
    ArgParse();

    ArgParse(ArgParse&& r_rhs) = default;

    ArgParse& operator=(ArgParse&& r_rhs) = default;

    /// Explicit constructor required by pimpl.
    ~ArgParse();

    /** Add a positional argument
     *
     *  A positional argument is a required argument that expects at least one value before
     *  any keywords or flags are specified. Since they are not optional, positional arguments
     *  have no default arguments.
     *
     *  @param r_name name of the argument. Must not be empty.
     *  @param nArgs number of expected values. Must not be @ref{ArgumentCount::None} but may
     *               variable ( @ref{ArgumentCount::Any} or @ref{ArgumentCount::NonZero} ).
     *  @param r_validator functor that validates each parsed value.
     *  @param r_docString description/help for the variable.
     */
    void addPositional(std::string&& r_name,
                       ArgumentCount nArgs           = ArgumentCount::One,
                       const Validator& r_validator  = defaultValidator,
                       DefaultValue&& r_defaultValue = {},
                       std::string&& r_docString     = "");

    /** Add a keyword argument
     *
     *  A keyword is an optional or required argument that is identified by at least one key,
     *  and may expect any number of values except 0. Values of keyword arguments must follow
     *  their respective keys. All passed keys must match one of two key patterns:
     *  - beginning with '-' followed by a single English character [a-zA-Z]
     *  - beginning with '--' followed by a single English character [a-zA-Z] then any number of alphanumeric characters [a-zA-Z0-9]
     *
     *  @param r_keys container of keys with at least one item
     *  @param r_name the variable's name. If empty, the first key is used.
     *  @param isOptional indicates whether the argument is optional.
                          If false, @ref{parseArguments} throws an error if the argument
     *                    is not found in the input.
     *  @param nArgs indicates how many values the argument expects. Pass @ref{ArgumentCount::Any}
     *               or @ref{ArgumentCount::NonZero} for a variable number of values.
     *  @param r_validator functor that validates each parsed value.
     *  @param r_defaultValue container of default values which are used if the argument
     *                        is not found during parsing. Non-optional arguments must not
     *                        have default values. If the argument is optional, the number
     *                        of default values must match @ref{nArgs}.
     *  @param r_docString description/help for the variable.
     */
    void addKeyword(KeyContainer&& r_keys,
                    std::string&& r_name          = "",
                    bool isOptional               = true,
                    ArgumentCount nArgs           = ArgumentCount::One,
                    const Validator& r_validator  = defaultValidator,
                    DefaultValue&& r_defaultValue = {},
                    std::string&& r_docString     = "");

    /** Add a flag
     *
     *  A flag is an optional variable that must not expect explicit values,
     *  but has a binary true/false value depending whether it is set or not respectively.
     *  It must have at least one key, and all passed keys must match one of two key patterns:
     *  - beginning with '-' followed by a single English character [a-zA-Z]
     *  - beginning with '--' followed by a single English character [a-zA-Z] then any number of alphanumeric characters [a-zA-Z0-9]
     *
     *  @param r_keys a container of keys with at least one item.
     *  @param r_name the argument's name. If empty, the first key is used.
     *  @param r_docString description/help for the variable.
     */
    void addFlag(KeyContainer&& r_keys,
                 std::string&& r_name      = "",
                 std::string&& r_docString = "");

    /** Deduce the argument type and add it to the list
     *
     *  Positional argument:
     *      - has exactly one key, and that is empty
     *      - is not optional
     *      - has no default values
     *      - can have any number of values except @ref{ArgumentCount::None}
     *
     *  Keyword Argument:
     *      - has at least one key, and it must match a key pattern
     *      - can be optional
     *      - may have default values
     *      - can have any number of values except @ref{ArgumentCount::None}
     *
     *  Flag argument:
     *      - has at least one key, and it must match a key pattern
     *      - can be optional
     *      - does not have a default value (but has a built-in false value if not set)
     *      - does not have any arguments
     *
     *  @param r_name unique name of the argument
     *  @param r_keys container of keys
     *  @param isOptional indicates whether the argument is optional.
     *                    If false, @ref{parseArguments} throws an error if the argument
     *                    is not found in the input.
     *  @param nArgs indicates how many values the argument expects. Pass @ref{ArgumentCount::Any}
     *               or @ref{ArgumentCount::NonZero} for a variable number of values, in which case
     *               values will be consumed greedily during parsing. Only one positional argument
     *               is allowed to have a variable number of arguments.
     *  @param r_validator functor that validates each parsed value
     *  @param r_defaultValue container of default values which are used if the argument
     *                        is not found during parsing. Non-optional arguments must not
     *                        have default values. If the argument is optional, the number
     *                        of default values must match @ref{nArgs}.
     *  @param r_docString description/help for the argument
     */
    void addArgument(std::string&& r_name,
                     KeyContainer&& r_keys,
                     bool isOptional,
                     ArgumentCount nArgs,
                     const Validator& r_validator,
                     DefaultValue&& r_defaultValue,
                     std::string&& r_docString);

    /** Parse the input arguments and return a map containing all parsed and default values
     *
     *  @param argc number of arguments
     *  @param argv array of arguments of size 'argc'
     *  @return {argument_name, argument_values}
     *
     *  Note: the returned map contains every registered argument
     */
    Results parseArguments(int argc, const char* argv[]) const;

private:
    ArgParse(const ArgParse& r_rhs) = delete;

    ArgParse& operator=(const ArgParse& r_rhs) = delete;

private:
    class Impl;
    std::unique_ptr<Impl> _p_impl;
}; // class ArgParse


} // namespace cie::utils


#endif