// --- Utility Includes ---
#include "cieutils/packages/commandline/inc/PositionalArgument.hpp"
#include "cieutils/packages/exceptions/inc/exception.hpp"
#include "cieutils/packages/macros/inc/exceptions.hpp"


namespace cie::utils::detail {


bool PositionalArgument::matchesKey(const ArgParse::KeyView& r_key) const
{
    return false;
}


bool PositionalArgument::matchesKey(const ArgParse::ValueView& r_key) const
{
    return false;
}


} // namespace cie::utils::detail