// --- Utility Includes ---
#include "cieutils/packages/commandline/inc/KeywordParser.hpp"
#include "cieutils/packages/macros/inc/exceptions.hpp"
#include "cieutils/packages/macros/inc/checks.hpp"


namespace cie::utils::detail {


ArgParse::Key KeywordParser::parse(const ArgParse::Key& r_key)
{
    std::smatch match;
    const bool isAMatch = std::regex_match(
        r_key.begin(),
        r_key.end(),
        match,
        KeywordParser::regex
    );

    CIE_CHECK(
        isAMatch && (match[1].matched != match[2].matched),
        "Invalid key for optional argument: '" + r_key + "'"
    )

    return r_key;
}


const std::regex KeywordParser::regex(R"(\-([a-zA-Z_])$|^\-\-([a-zA-Z_][a-zA-Z0-9\-_]*)$)");


} // namespace cie::utils::detail