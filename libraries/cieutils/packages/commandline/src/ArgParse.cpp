// --- Utility Incldues ---
#include "cieutils/packages/commandline/inc/ArgParse.hpp"
#include "cieutils/packages/commandline/inc/AbsArgument.hpp"
#include "cieutils/packages/commandline/inc/AggregateArgument.hpp"
#include "cieutils/packages/commandline/inc/PositionalArgument.hpp"
#include "cieutils/packages/commandline/inc/KeywordArgument.hpp"
#include "cieutils/packages/commandline/inc/FlagArgument.hpp"
#include "cieutils/packages/stl_extension/inc/DynamicArray.hpp"
#include "cieutils/packages/macros/inc/exceptions.hpp"
#include "cieutils/packages/macros/inc/checks.hpp"

// --- STL Includes ---
#include <unordered_set>
#include <algorithm>
#include <ranges>
#include <sstream>
#include <limits>
#include <iostream>


namespace cie::utils {


const ArgParse::ArgumentCount ArgParse::ArgumentCount::None(0);


const ArgParse::ArgumentCount ArgParse::ArgumentCount::One(1);


const ArgParse::ArgumentCount ArgParse::ArgumentCount::NonZero(-1);


const ArgParse::ArgumentCount ArgParse::ArgumentCount::Any(-2);


const ArgParse::Validator ArgParse::defaultValidator = [](const auto& r_value){return true;};


class ArgParse::Impl
{
public:
    using Argument = detail::AbsAggregateArgument;

    using ArgumentContainer = DynamicArray<Argument::SharedPointer>;

    ~Impl();

    ArgumentContainer _arguments;
}; // class ArgParse::Impl


ArgParse::Impl::~Impl()
{
}


ArgParse::ArgParse()
    : _p_impl(new ArgParse::Impl)
{
}


ArgParse::~ArgParse()
{
}


void ArgParse::addPositional(std::string&& r_name,
                             ArgumentCount nArgs,
                             const Validator& r_validator,
                             DefaultValue&& r_defaultValue,
                             std::string&& r_docString)
{
    this->addArgument(
        std::move(r_name),
        {""},
        false,
        nArgs,
        r_validator,
        std::move(r_defaultValue),
        std::move(r_docString)
    );
}


void ArgParse::addKeyword(KeyContainer&& r_keys,
                          std::string&& r_name,
                          bool isOptional,
                          ArgumentCount nArgs,
                          const Validator& r_validator,
                          DefaultValue&& r_defaultValue,
                          std::string&& r_docString)
{
    if (r_name.empty())
    {
        if (!r_keys.empty())
            r_name = r_keys.front();
        else
            CIE_THROW(Exception, "Keyword argument missing both name and key")
    }

    this->addArgument(
        std::move(r_name),
        std::move(r_keys),
        isOptional,
        nArgs,
        r_validator,
        std::move(r_defaultValue),
        std::move(r_docString)
    );
}


void ArgParse::addFlag(KeyContainer&& r_keys,
                       std::string&& r_name,
                       std::string&& r_docString)
{
    if (r_name.empty())
    {
        if (!r_keys.empty())
            r_name = r_keys.front();
        else
            CIE_THROW(Exception, "Keyword argument missing both name and key")
    }

    this->addArgument(
        std::move(r_name),
        std::move(r_keys),
        true,
        ArgumentCount::None,
        this->defaultValidator,
        {},
        std::move(r_docString)
    );
}


void ArgParse::addArgument(std::string&& r_name,
                           KeyContainer&& r_keys,
                           bool isOptional,
                           ArgumentCount nArgs,
                           const Validator& r_validator,
                           DefaultValue&& r_defaultValue,
                           std::string&& r_docString)
{
    CIE_BEGIN_EXCEPTION_TRACING

    // Check keys
    CIE_CHECK(
        !r_keys.empty(),
        "At least one key must be provided for each argument. Positional arguments are represented by empty strings for keys."
    )

    // Check argument name
    CIE_CHECK(
        !r_name.empty(),
        "Empty argument name"
    )

    for (const auto& rp_argument : _p_impl->_arguments)
    {
        CIE_CHECK(
            rp_argument->name() != r_name,
            "Argument with name '" + r_name + "' already exists"
        )
    }

    // Check whether the new argument's keys clash with existing ones
    const auto& r_arguments = _p_impl->_arguments;
    for (const auto& rp_argument : r_arguments)
        for (const auto& r_key : r_keys)
            CIE_CHECK(
                !rp_argument->matchesKey(r_key),
                "Key '" + r_key + "' of new argument '" + r_name + "' clashes with argument '" + rp_argument->name() + "'"
            )

    // Init
    const Size id = _p_impl->_arguments.size();
    auto tag = detail::AbsArgument::Tag::Invalid;

    // Get tag
    for (const auto& r_key : r_keys)
    {
        const auto localTag = Impl::Argument::parseTag(r_key, nArgs);
        CIE_CHECK(
            localTag != detail::AbsArgument::Tag::Invalid,
            "Invalid key '" + r_key + "'"
        )

        if (tag == detail::AbsArgument::Tag::Invalid)
            tag = localTag;
        else if (tag != localTag)
            CIE_THROW(Exception, "Argument type mismatch for key'" + r_key + "' in argument '" + r_name + "'")
    }

    // Construct arguments
    switch (tag)
    {
        case (detail::AbsArgument::Tag::Invalid):
        {
            CIE_THROW(Exception, "Invalid argument '" + r_name + "'")
            break;
        } // Invlaid
        case (detail::AbsArgument::Tag::Positional):
        {
            CIE_CHECK(
                r_keys.size() == 1,
                "Positional argument '" + r_name + "' must have exactly 1 key (and it must be empty)"
            )
            const auto& r_key = r_keys.front();
            CIE_CHECK(
                r_key.empty(),
                "Positional argument '" + r_name + "' has a non-empty key"
            )
            CIE_CHECK(
                r_defaultValue.empty(),
                "Positional argument '" + r_name + "' got default values"
            )

            // Check positional arguments' compatibility
            for (const auto& rp_argument : std::ranges::subrange(_p_impl->_arguments.rbegin(), _p_impl->_arguments.rend()))
                if (rp_argument->tag() == detail::AbsArgument::Tag::Positional)
                {
                    CIE_CHECK(
                        rp_argument->nArgs() != ArgumentCount::Any && rp_argument->nArgs() != ArgumentCount::NonZero,
                        "Attempt to add positional argument '" + r_name + "' after another positional argument '" + rp_argument->name() + "' with variable number of values"
                    )
                    break;
                }

            using ArgumentType = detail::PositionalArgument;
            using AggregateType = detail::AggregateArgument<ArgumentType>;
            AggregateType::ArgumentContainer arguments;
            arguments.emplace_back();

            _p_impl->_arguments.emplace_back(new AggregateType(
                std::move(arguments),
                id,
                isOptional,
                std::move(r_defaultValue),
                nArgs,
                r_validator,
                std::move(r_docString),
                std::move(r_name)
            ));
            break;
        } // Positional
        case (detail::AbsArgument::Tag::Keyword):
        {
            using ArgumentType = detail::KeywordArgument;
            using AggregateType = detail::AggregateArgument<ArgumentType>;
            AggregateType::ArgumentContainer arguments;
            arguments.reserve(r_keys.size());

            for (const auto& r_key : r_keys)
                arguments.emplace_back(r_key);

            _p_impl->_arguments.emplace_back(new AggregateType(
                std::move(arguments),
                id,
                isOptional,
                std::move(r_defaultValue),
                nArgs,
                r_validator,
                std::move(r_docString),
                std::move(r_name)
            ));
            break;
        } // Keyword
        case (detail::AbsArgument::Tag::Flag):
        {
            using ArgumentType = detail::FlagArgument;
            using AggregateType = detail::AggregateArgument<ArgumentType>;
            AggregateType::ArgumentContainer arguments;
            arguments.reserve(r_keys.size());

            for (const auto& r_key : r_keys)
                arguments.emplace_back(r_key);

            _p_impl->_arguments.emplace_back(new AggregateType(
                std::move(arguments),
                id,
                isOptional,
                std::move(r_defaultValue),
                nArgs,
                r_validator,
                std::move(r_docString),
                std::move(r_name)
            ));
            break;
        } // Flag
    } // switch tag

    CIE_END_EXCEPTION_TRACING
}


ArgParse::ValueView CStringToView(const char* begin)
{
    CIE_BEGIN_EXCEPTION_TRACING

    const char* end = begin;
    while (*end != '\0')
        ++end;

    return ArgParse::ValueView(begin, end);

    CIE_END_EXCEPTION_TRACING
}


// Collect values from the input until the required amount is reached
// or, if the argument expects a variable number of values, until a
// value matches one of the arguments. In the latter case, return the
// a pointer to the matched variable, otherwise return nullptr.
detail::AbsAggregateArgument::SharedConstPointer
consumeValues(int& argc,
              const char**& argv,
              const DynamicArray<detail::AbsAggregateArgument::SharedPointer> r_arguments,
              ArgParse::ValueViewContainer& r_views,
              int maxValues)
{
    CIE_BEGIN_EXCEPTION_TRACING

    detail::AbsAggregateArgument::SharedConstPointer p_matched;

    // Loop through values until:
    //  - end of values
    //  - consumed a fixed number of values (maxValues)
    //  - a value matches at least one of the arguments
    for (; 0 < argc && 0 < maxValues && !p_matched; --argc, --maxValues, ++argv)
    {
        // Create a view from the current value
        auto view = CStringToView(*argv);

        // If this value matches with any of the arguments,
        // it means that we've run out of values, or the input
        // is invalid (this case would handled caught later on).
        const auto it_argument = std::find_if(
            r_arguments.begin(),
            r_arguments.end(),
            [&view](const auto& rp_arg){return rp_arg->matchesKey(view);}
        );

        if (it_argument != r_arguments.end())
        {
            p_matched = *it_argument;
            break;
        }

        // Add a view of the current value
        r_views.emplace_back(std::move(view));
    }

    return p_matched;

    CIE_END_EXCEPTION_TRACING
}


ArgParse::Results ArgParse::parseArguments(int argc, const char* argv[]) const
{
    CIE_BEGIN_EXCEPTION_TRACING

    ArgParse::Results output;

    // Create a map that tracks whether an argument
    // appeared in the input
    std::unordered_map<Impl::Argument::SharedConstPointer,bool> argumentFoundMap;
    for (const auto& rp_argument : _p_impl->_arguments)
        argumentFoundMap.emplace(rp_argument, false);

    // Collect positional arguments
    DynamicArray<Impl::Argument::SharedConstPointer> positionals;
    {
        int expectedPositionalCount = 0;

        for (const auto& rp_argument : _p_impl->_arguments)
            if (rp_argument->tag() == detail::AbsArgument::Tag::Positional)
            {
                positionals.emplace_back(rp_argument);
                const auto nArgs = rp_argument->nArgs();
                if (nArgs == ArgumentCount::Any || nArgs == ArgumentCount::NonZero)
                {
                    expectedPositionalCount = argc;
                    break;
                } // variable argument count
                else
                {
                    expectedPositionalCount += int(nArgs);
                } // Exact argument count
            } // if positional argument

        // Check whether the expected number of positional values is valid
        CIE_CHECK(
            expectedPositionalCount <= argc,
            (std::stringstream() << "Expecting " << expectedPositionalCount << " positional arguments, but " << argc << " were given in total").str()
        )
    } // construct separators

    // Assign values to positional arguments
    ValueViewContainer views;

    for (const auto& rp_positional : positionals)
    {
        const auto nArgs = rp_positional->nArgs();
        views.clear();

        // Consume values
        // Note: argc and argv are updated as values are consumed
        auto p_matched = consumeValues(
            argc,
            argv,
            _p_impl->_arguments,
            views,
            nArgs != ArgumentCount::Any && nArgs != ArgumentCount::None ? int(nArgs) : argc
        );

        // A value matched an argument => end of positionals or error
        if (p_matched)
            break;

        // Pass the collected views to the positional argument
        // which will validate them. Then push the validated
        // values to the output map.
        output.emplace(rp_positional->name(), rp_positional->parseValues(views));
        argumentFoundMap[rp_positional] = true;
    }

    // Assign values to keyword and flag arguments
    while (0 < argc)
    {
        // Assume the current value is a key

        // Get argument matching the key.
        // Possible results:
        //  - 'views' is empty and 'p_matched' is valid:
        //    argument is found and no additional values are consumed (OK)
        //
        //  - 'views' is not empty and 'p_matched' is valid:
        //    unexpected positional arguments were found before finding a keyword (INPUT ERROR)
        //
        //  - 'views' is empty and 'p_matched' is invalid:
        //    this should not happen and means there is a bug somewhere (ERROR)
        //
        //  - 'views' is not empty and 'p_matched' is invalid
        //    unexpected trailing values (INPUT ERROR)
        views.clear();
        auto p_matched = consumeValues(
            argc,
            argv,
            _p_impl->_arguments,
            views,
            argc
        );

        if (!views.empty())
        {
            std::stringstream stream;
            stream << "Unexpected arguments or unrecognized keys: ";
            for (const auto& r_view : views)
                stream << "'" << std::string(r_view.begin(), r_view.end()) << "' ";
            CIE_THROW(Exception, stream.str())
        }

        CIE_CHECK(
            p_matched,
            "Expecting a key but found none"
        )

        // From here on, 'p_matched' is valid and 'views' is empty.
        // => move on to the values (if any)
        --argc;
        ++argv;

        // Collect values
        const auto nArgs = p_matched->nArgs();
        consumeValues(
            argc,
            argv,
            _p_impl->_arguments,
            views,
            nArgs != ArgumentCount::Any && nArgs != ArgumentCount::None ? int(nArgs) : argc
        );

        // Pass the collected views to the argument
        // which will validate them. Then push the validated
        // values to the output map.
        auto emplaceResult = output.emplace(p_matched->name(), p_matched->parseValues(views));

        if (!emplaceResult.second)
            CIE_THROW(Exception, "Duplicate key: '" + p_matched->name() + "'")

        argumentFoundMap[p_matched] = true;
    } // for i_argument < argc

    // Check whether all required arguments were found
    // and assign default values to arguments that were not found.
    for (const auto& r_pair : argumentFoundMap)
    {
        if (!r_pair.second)
        {
            if (!r_pair.first->isOptional())
                CIE_THROW(Exception, "Required argument '" + r_pair.first->name() +"' was not found")
            else if (r_pair.first->tag() == detail::AbsArgument::Tag::Flag)
                output[r_pair.first->name()] = {"false"};
            else
                output[r_pair.first->name()] = r_pair.first->defaultValue();
        }
        else if (r_pair.first->tag() == detail::AbsArgument::Tag::Flag)
            output[r_pair.first->name()] = {"true"};
    }

    return output;

    CIE_END_EXCEPTION_TRACING
}


} // namespace cie::utils