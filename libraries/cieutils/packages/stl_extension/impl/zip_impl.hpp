#ifndef CIE_UTILS_STL_EXTENSION_ZIP_IMPL_HPP
#define CIE_UTILS_STL_EXTENSION_ZIP_IMPL_HPP


#include <utility>
namespace cie::utils {


namespace detail {


template <class ...TContainers>
template <class TValue>
inline TValue
ZipIterator<TContainers...>::makeReferenceTuple()
{
    return this->makeReferenceTupleImpl<TValue>(
        std::make_index_sequence<sizeof...(TContainers)>()
    );
}


template <class ...TContainers>
template <class TValue, std::size_t ...Indices>
inline TValue
ZipIterator<TContainers...>::makeReferenceTupleImpl(std::index_sequence<Indices...> sequence)
{
    return TValue(*std::get<Indices>(_it)...);
}


template <class ...TContainers>
template <std::size_t ...Indices>
inline typename ZipObject<TContainers...>::Iterator
ZipObject<TContainers...>::beginImpl(std::index_sequence<Indices...> sequence)
{
    using Iterator = typename ZipObject<TContainers...>::Iterator;
    return Iterator(
        typename Iterator::IteratorTuple(std::get<Indices>(_containers).begin()...)
    );
}


template <class ...TContainers>
template <std::size_t ...Indices>
inline typename ZipObject<TContainers...>::Iterator
ZipObject<TContainers...>::endImpl(std::index_sequence<Indices...> sequence)
{
    const Size minSize = ZipObject<TContainers...>::MinimumSize<sizeof...(TContainers)-1>::get(_containers);
    auto it = this->begin();
    it += minSize;
    return it;
}


} // namespace detail


} // namespace cie::utils


#endif