#ifndef CIE_UTILS_STL_EXTENSION_STATIC_ARRAY_HPP
#define CIE_UTILS_STL_EXTENSION_STATIC_ARRAY_HPP

// --- Internal Includes ---
#include "cieutils/packages/stl_extension/inc/STLContainerBase.hpp"

// --- STL Includes ---
#include <array>


namespace cie {


template <class ValueType, Size ArraySize>
using StaticArray = STLContainerBase<std::array<ValueType,ArraySize>>;


} // namespace cie


#endif