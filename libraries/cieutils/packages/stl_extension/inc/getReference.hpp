#ifndef CIE_UTILS_STL_EXTENSION_GET_REFERENCE_HPP
#define CIE_UTILS_STL_EXTENSION_GET_REFERENCE_HPP

// --- Utility Includes ---
#include "cieutils/packages/compile_time/packages/concepts/inc/basic_concepts.hpp"
#include "cieutils/packages/macros/inc/checks.hpp"

// --- STL Includes ---
#include <memory>


namespace cie::utils {


template <class T>
requires concepts::Pointer<T>
inline typename std::pointer_traits<typename std::decay<T>::type>::element_type&
getReference(T& r_p)
{
    CIE_CHECK_POINTER(r_p)
    return *r_p;
}


template <class T>
requires concepts::Pointer<T>
inline const typename std::pointer_traits<typename std::decay<T>::type>::element_type&
getReference(const T& r_p)
{
    CIE_CHECK_POINTER(r_p)
    return *r_p;
}


template <class T>
requires concepts::NonPointer<T>
inline T&
getReference(T& r)
{
    return r;
}


template <class T>
requires concepts::NonPointer<T>
inline const T&
getReference(const T& r)
{
    return r;
}


} // namespace cie::utils


#endif