#ifndef CIE_UTILS_STL_EXTENSION_DYNAMIC_ARRAY_HPP
#define CIE_UTILS_STL_EXTENSION_DYNAMIC_ARRAY_HPP

// --- Internal Includes ---
#include "cieutils/packages/stl_extension/inc/STLContainerBase.hpp"

// --- STL Includes ---
#include <vector>


namespace cie {


template <class ValueType, class ...Arguments>
using DynamicArray = STLContainerBase<std::vector<ValueType,Arguments...>>;


} // namespace cie


#endif