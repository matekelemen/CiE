#ifndef CIE_UTILS_PARALLEL_FOR_IMPL_HPP
#define CIE_UTILS_PARALLEL_FOR_IMPL_HPP

// --- Utility Includes ---
#include "cieutils/packages/concurrency/inc/ThreadPoolSingleton.hpp"
#include "cieutils/packages/macros/inc/exceptions.hpp"
#include "cieutils/packages/macros/inc/checks.hpp"


namespace cie::mp {


template <concepts::Integer TIndex, class TStorage>
template <class ...Args>
requires (!std::is_same_v<std::tuple<typename std::decay<Args>::type...>,std::tuple<ParallelFor<TIndex,TStorage>>>)
ParallelFor<TIndex,TStorage>::ParallelFor(Args&&... r_args) :
    _p_pool(ThreadPoolSingleton::firstPrivate(std::forward<Args>(r_args)...))
{
}


template <concepts::Integer TIndex, class TStorage>
template <class ...Args>
inline ParallelFor<TIndex,ThreadStorage<TIndex,typename std::remove_reference<Args>::type...>>
ParallelFor<TIndex,TStorage>::firstPrivate(Args&&... r_args)
{
    CIE_BEGIN_EXCEPTION_TRACING

    return ParallelFor<TIndex,ThreadStorage<TIndex,typename std::remove_reference<Args>::type...>>(std::forward<Args>(r_args)...);

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Integer TIndex, class TStorage>
ParallelFor<TIndex,TStorage>&
ParallelFor<TIndex,TStorage>::setPool(typename ParallelFor<TIndex,TStorage>::Pool::SharedPointer p_pool)
{
    this->_p_pool = p_pool;
    return *this;
}


template <concepts::Integer TIndex, class TStorage>
inline ParallelFor<TIndex,TStorage>&
ParallelFor<TIndex,TStorage>::operator()(TIndex indexMin,
                                         TIndex indexMax,
                                         TIndex stepSize,
                                         typename TStorage::IndexLoopFunction&& r_function)
{
    CIE_BEGIN_EXCEPTION_TRACING

    this->execute(
        std::move(this->makeIndexPartition(indexMin,indexMax,stepSize)),
        stepSize,
        std::forward<typename TStorage::IndexLoopFunction>(r_function)
    );

    return *this;

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Integer TIndex, class TStorage>
inline ParallelFor<TIndex,TStorage>&
ParallelFor<TIndex,TStorage>::operator()(TIndex indexMax,
                                         typename TStorage::IndexLoopFunction&& r_function)
{
    CIE_BEGIN_EXCEPTION_TRACING

    this->execute(
        std::move(this->makeIndexPartition(0, indexMax, 1)),
        1,
        std::forward<typename TStorage::IndexLoopFunction>(r_function)
    );

    return *this;

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Integer TIndex, class TStorage>
template <concepts::STLContainer TContainer>
inline ParallelFor<TIndex,TStorage>&
ParallelFor<TIndex,TStorage>::operator()(TContainer& r_container,
                                         typename TStorage::ObjectLoopFunction<typename TContainer::value_type>&& r_function)
{
    CIE_BEGIN_EXCEPTION_TRACING

    this->execute(
        std::move(this->makeIndexPartition(0,r_container.size(),1)),
        1,
        r_container,
        std::forward<typename TStorage::ObjectLoopFunction<typename TContainer::value_type>>(r_function)
    );

    return *this;

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Integer TIndex, class TStorage>
template <concepts::STLContainer TContainer>
inline ParallelFor<TIndex,TStorage>&
ParallelFor<TIndex,TStorage>::operator()(const TContainer& r_container,
                                         typename TStorage::ConstObjectLoopFunction<typename TContainer::value_type>&& r_function)
{
    CIE_BEGIN_EXCEPTION_TRACING

    this->execute(
        std::move(this->makeIndexPartition(0,r_container.size(),1)),
        1,
        r_container,
        std::forward<typename TStorage::ObjectLoopFunction<typename TContainer::value_type>>(r_function)
    );

    return *this;

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Integer TIndex, class TStorage>
template <concepts::STLContainer TContainer>
inline ParallelFor<TIndex,TStorage>&
ParallelFor<TIndex,TStorage>::operator()(TContainer& r_container,
                                         typename TStorage::ObjectLoopFunction<typename TContainer::value_type>& r_function)
{
    CIE_BEGIN_EXCEPTION_TRACING

    this->execute(
        std::move(this->makeIndexPartition(0,r_container.size(),1)),
        1,
        r_container,
        std::forward<typename TStorage::ObjectLoopFunction<typename TContainer::value_type>>(r_function)
    );

    return *this;

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Integer TIndex, class TStorage>
template <concepts::STLContainer TContainer>
inline ParallelFor<TIndex,TStorage>&
ParallelFor<TIndex,TStorage>::operator()(const TContainer& r_container,
                                         typename TStorage::ConstObjectLoopFunction<typename TContainer::value_type>& r_function)
{
    CIE_BEGIN_EXCEPTION_TRACING

    this->execute(
        std::move(this->makeIndexPartition(0,r_container.size(),1)),
        1,
        r_container,
        std::forward<typename TStorage::ObjectLoopFunction<typename TContainer::value_type>>(r_function)
    );

    return *this;

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Integer TIndex, class TStorage>
inline const ThreadPoolStorage<TStorage>&
ParallelFor<TIndex,TStorage>::getStorage() const
{
    return _p_pool->getStorage();
}


template <concepts::Integer TIndex, class TStorage>
typename ParallelFor<TIndex,TStorage>::IndexPartition
ParallelFor<TIndex,TStorage>::makeIndexPartition(TIndex indexMin,
                                                 TIndex indexMax,
                                                 TIndex stepSize)
{
    CIE_BEGIN_EXCEPTION_TRACING

    // Create a pool if one wasn't provided
    if (!this->_p_pool)
        this->_p_pool.reset(new Pool);

    // Check index range
    CIE_OUT_OF_RANGE_CHECK(stepSize != 0)
    CIE_OUT_OF_RANGE_CHECK((indexMin <= indexMax) == (0 < stepSize));

    // Handle reverse loops
    if (stepSize < 0)
    {
        auto tmp = indexMin;
        indexMin = indexMax;
        indexMax = tmp;
        stepSize = 0 - stepSize;
    }

    typename ParallelFor<TIndex,TStorage>::IndexPartition indexPartition;

    // Initialize index blocks
    TIndex blockSize = stepSize * (indexMax - indexMin) / (stepSize * this->_p_pool->size() + 1);

    if (blockSize == 0)
        blockSize = 1;

    TIndex tmp = indexMin;

    indexPartition.reserve(this->_p_pool->size() + 1);
    for (TIndex i=0; i<this->_p_pool->size() && tmp<indexMax; ++i, tmp+=blockSize)
        indexPartition.push_back(tmp);

    indexPartition.push_back(indexMax);

    return indexPartition;

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Integer TIndex, class TStorage>
void
ParallelFor<TIndex,TStorage>::execute(const typename ParallelFor<TIndex,TStorage>::IndexPartition& r_indexPartition,
                                      TIndex stepSize,
                                      typename ParallelFor<TIndex,TStorage>::IndexLoopFunction&& r_function)
{
    CIE_BEGIN_EXCEPTION_TRACING

    CIE_OUT_OF_RANGE_CHECK(r_indexPartition.size() <= this->_p_pool->size() + 1)

    // Schedule jobs
    for (TIndex partitionIndex=0; partitionIndex<r_indexPartition.size()-1; ++partitionIndex)
    {
        this->_p_pool->queueJob(
            [partitionIndex, stepSize, r_indexPartition, r_function](auto&... r_args) -> void
            {
                for (TIndex i=r_indexPartition[partitionIndex]; i<r_indexPartition[partitionIndex+1]; i+=stepSize)
                    r_function(i, r_args...);
            }
        ); // queueJob
    }

    this->_p_pool->barrier();

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Integer TIndex, class TStorage>
template <concepts::STLContainer TContainer>
void
ParallelFor<TIndex,TStorage>::execute(const typename ParallelFor<TIndex,TStorage>::IndexPartition& r_indexPartition,
                                      TIndex stepSize,
                                      TContainer& r_container,
                                      typename ParallelFor<TIndex,TStorage>::ObjectLoopFunction<typename TContainer::value_type>&& r_function)
{
    CIE_BEGIN_EXCEPTION_TRACING

    CIE_OUT_OF_RANGE_CHECK(r_indexPartition.size() <= this->_p_pool->size() + 1)

    // Schedule jobs
    for (TIndex partitionIndex=0; partitionIndex<r_indexPartition.size()-1; ++partitionIndex)
    {
        this->_p_pool->queueJob(
            [partitionIndex, stepSize, r_indexPartition, r_function, &r_container](auto&... r_args) -> void
            {
                for (TIndex i=r_indexPartition[partitionIndex]; i<r_indexPartition[partitionIndex+1]; i+=stepSize)
                    r_function(r_container[i], r_args...);
            }
        ); // queueJob
    }

    this->_p_pool->barrier();

    CIE_END_EXCEPTION_TRACING
}


} // namespace cie::mp


#endif