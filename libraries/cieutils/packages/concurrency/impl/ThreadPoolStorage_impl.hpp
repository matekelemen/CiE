#ifndef CIE_UTILS_THREAD_POOL_STORAGE_IMPL_HPP
#define CIE_UTILS_THREAD_POOL_STORAGE_IMPL_HPP


#include "cieutils/packages/macros/inc/checks.hpp"
namespace cie::mp {


template <class TStorage>
TStorage& ThreadPoolStorage<TStorage>::getLocalStorage()
{
    CIE_CHECK_POINTER(_p_pool)
    CIE_OUT_OF_RANGE_CHECK(_p_pool->threadIndex() < _p_pool->size())
    return _storage[_p_pool->threadIndex()];
}


template <class TStorage>
const TStorage& ThreadPoolStorage<TStorage>::getLocalStorage() const
{
    CIE_CHECK_POINTER(_p_pool)
    CIE_OUT_OF_RANGE_CHECK(_p_pool->threadIndex() < _p_pool->size())
    return _storage[_p_pool->threadIndex()];
}


template <class TStorage>
void ThreadPoolStorage<TStorage>::setLocalStorage(TStorage&& r_storage)
{
    CIE_CHECK_POINTER(_p_pool)
    CIE_OUT_OF_RANGE_CHECK(_p_pool->threadIndex() < _p_pool->size())
    _storage[_p_pool->threadIndex()] = r_storage;
}


template <class TStorage>
void ThreadPoolStorage<TStorage>::setLocalStorage(const TStorage& r_storage)
{
    CIE_CHECK_POINTER(_p_pool)
    CIE_OUT_OF_RANGE_CHECK(_p_pool->threadIndex() < _p_pool->size())
    _storage[_p_pool->threadIndex()] = r_storage;
}


} // namespace cie::mp


#endif