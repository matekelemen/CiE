#ifndef CIE_UTILS_THREAD_POOL_IMPL_HPP
#define CIE_UTILS_THREAD_POOL_IMPL_HPP


namespace cie::mp {


template <class TStorage>
ThreadPool<TStorage>::ThreadPool()
    : ThreadPool(ThreadPoolBase::SharedPointer(new ThreadPoolBase))
{
}


template <class TStorage>
ThreadPool<TStorage>::ThreadPool(ThreadPoolBase::SharedPointer p_pool)
    : ThreadPool(p_pool, TStorage())
{
}


template <class TStorage>
ThreadPool<TStorage>::ThreadPool(ThreadPoolBase::SharedPointer p_pool,
                                 TStorage&& r_storage)
    : _p_pool(p_pool),
      _storage(p_pool, std::move(r_storage))
{
}


template <class TStorage>
ThreadPool<TStorage>::ThreadPool(ThreadPoolBase::SharedPointer p_pool,
                                 const TStorage& r_storage)
    : _p_pool(p_pool),
      _storage(p_pool, r_storage)
{
}


template <class TStorage>
inline Size
ThreadPool<TStorage>::maxNumberOfThreads()
{
    return ThreadPoolBase::maxNumberOfThreads();
}


template <class TStorage>
inline void
ThreadPool<TStorage>::queueJob(const typename ThreadPool<TStorage>::JobType& r_job)
{
    _p_pool->queueJob(
        [this, r_job]()
        {
            std::apply(
                [&r_job](auto&... r_args) -> void {r_job(r_args...);},
                _storage.getLocalStorage().getReferences()
            );
        }
    );
}


template <class TStorage>
inline Size
ThreadPool<TStorage>::size() const
{
    return _p_pool->size();
}


template <class TStorage>
inline Size
ThreadPool<TStorage>::numberOfJobs() const
{
    return _p_pool->numberOfJobs();
}


template <class TStorage>
void
ThreadPool<TStorage>::barrier()
{
    _p_pool->barrier();
}


template <class TStorage>
void
ThreadPool<TStorage>::terminate()
{
    _p_pool->terminate();
}


template <class TStorage>
inline ThreadPoolStorage<TStorage>&
ThreadPool<TStorage>::getStorage()
{
    return _storage;
}


template <class TStorage>
inline const ThreadPoolStorage<TStorage>&
ThreadPool<TStorage>::getStorage() const
{
    return _storage;
}


} // namespace cie::fem


#endif