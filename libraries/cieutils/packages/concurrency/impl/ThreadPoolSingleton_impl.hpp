#ifndef CIE_UTILS_THREAD_POOL_SINGLETON_IMPL_HPP
#define CIE_UTILS_THREAD_POOL_SINGLETON_IMPL_HPP

// --- Utility Includes ---
#include "cieutils/packages/macros/inc/exceptions.hpp"


namespace cie::mp {


template <class ...TArgs>
typename ThreadPool<ThreadStorage<Size,typename std::remove_reference<TArgs>::type...>>::SharedPointer
ThreadPoolSingleton::firstPrivate(TArgs&&... r_args)
{
    CIE_BEGIN_EXCEPTION_TRACING

    using StorageType = ThreadStorage<Size,typename std::remove_reference<TArgs>::type...>;

    if (!_p_pool)
        _p_pool.reset(new ThreadPoolBase);

    return typename ThreadPool<StorageType>::SharedPointer(
        new ThreadPool<StorageType>(
            _p_pool,
            StorageType(std::forward<TArgs>(r_args)...)
        )
    );

    CIE_END_EXCEPTION_TRACING
}


} // namespace cie::mp


#endif