// --- Internal Includes ---
#include "cieutils/packages/concurrency/inc/ThreadPoolSingleton.hpp"


namespace cie::mp {


ThreadPoolBase::SharedPointer ThreadPoolSingleton::_p_pool = nullptr;


ThreadPoolBase::SharedPointer ThreadPoolSingleton::getBase()
{
    auto& rp_pool = ThreadPoolSingleton::_p_pool;

    if (!rp_pool)
        rp_pool.reset(new ThreadPoolBase);

    return rp_pool;
}


ThreadPool<>::SharedPointer ThreadPoolSingleton::get()
{
    auto p_base = ThreadPoolSingleton::getBase();

    return typename ThreadPool<>::SharedPointer(
        new ThreadPool<>(p_base)
    );
}


} // namespace cie::mp