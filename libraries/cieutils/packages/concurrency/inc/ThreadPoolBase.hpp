#ifndef CIE_UTILS_CONCURRENCY_THREAD_POOL_BASE_HPP
#define CIE_UTILS_CONCURRENCY_THREAD_POOL_BASE_HPP

// --- Internal Includes ---
#include "cieutils/packages/types/inc/types.hpp"
#include "cieutils/packages/macros/inc/typedefs.hpp"

// --- STL Includes ---
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <queue>
#include <functional>
#include <memory>


namespace cie::mp {


class ThreadPoolBase final
{
public:
    using thread_type      = std::thread;

    using job_type         = std::function<void()>;

    using thread_container = std::vector<thread_type>;

    using job_container    = std::queue<job_type>;

    using mutex_type       = std::mutex;

    CIE_DEFINE_CLASS_POINTERS(ThreadPoolBase)

public:
    /// Create a thread pool with 'size' threads
    /**
     * @note 'size' must be positive and not greater than the maximum number
     * of threads on the system. The number of threads is capped at the supported
     * max.
     */
    ThreadPoolBase(Size size);

    /// Create a thread pool with the maximum number of threads the system supports
    ThreadPoolBase();

    ~ThreadPoolBase();

    static Size maxNumberOfThreads();

    /// Queue a job for execution
    void queueJob(job_type job);

    /// Number of threads in the pool
    Size size() const;

    /// Number of jobs in the queue
    Size numberOfJobs() const;

    /// TODO
    void barrier();

    void terminate();

    Size threadIndex() const;

private:
    ThreadPoolBase(ThreadPoolBase&& r_rhs) = delete;

    ThreadPoolBase(const ThreadPoolBase& r_rhs) = delete;

    ThreadPoolBase& operator=(ThreadPoolBase&& r_rhs) = delete;

    ThreadPoolBase& operator=(const ThreadPoolBase& r_rhs) = delete;

    /**
     * @brief Looping function running on all threads
     * Upon notification, sets the lock and checks the job queue,
     * strips and executes one if there are any.
     */
    void jobScheduler();

private:
    bool _terminate;

    thread_container _threads;

    job_container _jobs;

    mutex_type _mutex;

    std::condition_variable _jobCondition;

    std::condition_variable _barrierCondition;

    std::condition_variable _masterCondition;

    Size _numberOfActiveThreads;

    bool _barrier;
};


} // namespace cie::mp


#endif