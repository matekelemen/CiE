#ifndef CIE_UTILS_CONCURRENCY_THREAD_STORAGE_HPP
#define CIE_UTILS_CONCURRENCY_THREAD_STORAGE_HPP

// --- Internal Includes ---
#include "cieutils/packages/compile_time/packages/concepts/inc/basic_concepts.hpp"
#include "cieutils/packages/types/inc/types.hpp"

// --- STL Includes ---
#include <tuple>
#include <functional>


namespace cie::mp {


template <concepts::Integer TIndex, class ...TArgs>
class ThreadStorage
{
public:
    using IndexType = TIndex;

    using StorageType = std::tuple<TArgs...>;

    using ReferenceStorage = std::tuple<TArgs&...>;

    using ConstReferenceStorage = std::tuple<const TArgs&...>;

    template <class TReturn>
    using Function = std::function<TReturn(TArgs&...)>;

    template <class TReturn>
    using ConstFunction = std::function<TReturn(const TArgs&...)>;

    using IndexLoopFunction = std::function<void(IndexType,TArgs&...)>;

    using ConstIndexLoopFunction = std::function<void(IndexType,const TArgs&...)>;

    template <class TValue>
    using ObjectLoopFunction = std::function<void(TValue&,TArgs&...)>;

    template <class TValue>
    using ConstObjectLoopFunction = std::function<void(const TValue&, TArgs&...)>;

public:
    ThreadStorage()
    requires (!std::is_same_v<ThreadStorage<TIndex,TArgs...>,ThreadStorage<TIndex>>)
        : _values()
    {}

    template <class ...Ts>
    ThreadStorage(Ts&&... r_args) :
        _values(std::forward<Ts>(r_args)...)
    {}

    ThreadStorage(const ThreadStorage& r_rhs) = default;

    ThreadStorage(ThreadStorage&& r_rhs) = default;

    ThreadStorage& operator=(ThreadStorage&& r_rhs) = default;

    ThreadStorage& operator=(const ThreadStorage& r_rhs) = default;

    ReferenceStorage getReferences()
    {return std::make_from_tuple<ReferenceStorage>(_values);}

    ConstReferenceStorage getReferences() const
    {return std::make_from_tuple<ConstReferenceStorage>(_values);}

    template <Size Index>
    typename std::tuple_element<Index,StorageType>::type& get()
    {return std::get<Index>(_values);}

    template <Size Index>
    const typename std::tuple_element<Index,StorageType>::type& get() const
    {return std::get<Index>(_values);}

    StorageType& values()
    {return _values;}

    const StorageType& values() const
    {return _values;}

private:
    StorageType _values;
};


} // namespace cie::mp


#endif