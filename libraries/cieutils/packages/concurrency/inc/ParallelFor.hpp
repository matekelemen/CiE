#ifndef CIE_UTILS_PARALLEL_FOR_HPP
#define CIE_UTILS_PARALLEL_FOR_HPP

// --- Internal Includes ---
#include "cieutils/packages/concurrency/inc/ThreadStorage.hpp"
#include "cieutils/packages/concurrency/inc/ThreadPool.hpp"
#include "cieutils/packages/compile_time/packages/concepts/inc/container_concepts.hpp"
#include "cieutils/packages/types/inc/types.hpp"

// --- STL Includes ---
#include <functional>
#include <thread>
#include <vector>


namespace cie::mp {


template <concepts::Integer TIndex = Size,
          class TStorage = ThreadStorage<TIndex>>
class ParallelFor final
{
private:
    using IndexPartition = std::vector<TIndex>;

    using Storage = TStorage;

    using IndexLoopFunction = typename Storage::IndexLoopFunction;

    using ConstIndexLoopFunction = typename Storage::ConstIndexLoopFunction;

    template <class T>
    using ObjectLoopFunction = typename Storage::template ObjectLoopFunction<T>;

    template <class T>
    using ConstObjectLoopFunction = typename Storage::template ConstObjectLoopFunction<T>;

    using Pool = ThreadPool<Storage>;

public:
    ParallelFor(ParallelFor&& r_rhs) = default;

    ParallelFor& operator=(ParallelFor&& r_rhs) = default;

    template <class ...Args>
    requires (!std::is_same_v<std::tuple<typename std::decay<Args>::type...>,std::tuple<ParallelFor<TIndex,TStorage>>>)
    ParallelFor(Args&&... r_args);

    /** Create a parallel for construct with initialized thread-local variables.
     *
     *  The provided arguments are copied to each thread's local storage,
     *  and can be accessed only by its owner thread. Target functions must
     *  accept all arguments in matching order (after the index or container item).
     */
    template <class ...Args>
    static ParallelFor<TIndex,ThreadStorage<TIndex,typename std::remove_reference<Args>::type...>>
    firstPrivate(Args&&... r_args);

    /// Set the ThreadPool that will be used during the for loop.
    ParallelFor<TIndex,TStorage>& setPool(typename Pool::SharedPointer p_pool);

    /** Execute an indexed for loop.
     *
     *  @param indexMin index begin
     *  @param indexMax index end
     *  @param stepSize loop index increment value
     *  @param r_function target function to execute at each iteration
     */
    ParallelFor& operator()(TIndex indexMin,
                            TIndex indexMax,
                            TIndex stepSize,
                            typename TStorage::IndexLoopFunction&& r_function);

    /** Execute an indexed for loop beginning from 0.
     *
     *  @param indexMax index end
     *  @param r_function target function to execute at each iteration
     */
    ParallelFor& operator()(TIndex indexMax,
                            typename TStorage::IndexLoopFunction&& r_function);

    /** Execute a range-based for loop over a container.
     *
     *  @param r_container container holding the items to loop through
     *  @param r_function target function to execute at each iteration
     */
    template <concepts::STLContainer TContainer>
    ParallelFor& operator()(TContainer& r_container,
                            ObjectLoopFunction<typename TContainer::value_type>&& r_function);

    /** Execute a range-based for loop over a const container.
     *
     *  @param r_container container holding the items to loop through
     *  @param r_function target function to execute at each iteration
     */
    template <concepts::STLContainer TContainer>
    ParallelFor& operator()(const TContainer& r_container,
                            ConstObjectLoopFunction<typename TContainer::value_type>&& r_function);

    /** Execute a range-based for loop over a container.
     *
     *  @param r_container container holding the items to loop through
     *  @param r_function target function to execute at each iteration
     */
    template <concepts::STLContainer TContainer>
    ParallelFor& operator()(TContainer& r_container,
                            ObjectLoopFunction<typename TContainer::value_type>& r_function);

    /** Execute a range-based for loop over a const container.
     *
     *  @param r_container container holding the items to loop through
     *  @param r_function target function to execute at each iteration
     */
    template <concepts::STLContainer TContainer>
    ParallelFor& operator()(const TContainer& r_container,
                            ConstObjectLoopFunction<typename TContainer::value_type>& r_function);

    const ThreadPoolStorage<TStorage>& getStorage() const;

protected:
    IndexPartition makeIndexPartition(TIndex indexMin,
                                      TIndex indexMax,
                                      TIndex stepSize);

    void execute(const IndexPartition& r_indexPartition,
                 TIndex stepSize,
                 IndexLoopFunction&& r_function);

    template <concepts::STLContainer TContainer>
    void execute(const IndexPartition& r_indexPartition,
                 TIndex stepSize,
                 TContainer& r_container,
                 ObjectLoopFunction<typename TContainer::value_type>&& r_function);

private:
    typename Pool::SharedPointer _p_pool;
};


} // namespace cie::mp

#include "cieutils/packages/concurrency/impl/ParallelFor_impl.hpp"

#endif