#ifndef CIE_UTILS_THREAD_POOL_SINGLETON_HPP
#define CIE_UTILS_THREAD_POOL_SINGLETON_HPP

// --- Utility Includes ---
#include "cieutils/packages/concurrency/inc/ThreadPoolBase.hpp"
#include "cieutils/packages/concurrency/inc/ThreadPool.hpp"


namespace cie::mp {


class ThreadPoolSingleton
{
public:
    [[nodiscard]] static ThreadPoolBase::SharedPointer getBase();

    [[nodiscard]] static ThreadPool<>::SharedPointer get();

    template <class ...TArgs>
    [[nodiscard]] static typename ThreadPool<ThreadStorage<Size,typename std::remove_reference<TArgs>::type...>>::SharedPointer firstPrivate(TArgs&&... r_args);

private:
    static ThreadPoolBase::SharedPointer _p_pool;
}; // class ThreadPoolSingleton


} // namespace cie::mp

#include "cieutils/packages/concurrency/impl/ThreadPoolSingleton_impl.hpp"

#endif