#ifndef CIE_UTILS_THREAD_POOL_STORAGE_HPP
#define CIE_UTILS_THREAD_POOL_STORAGE_HPP

// --- Utility Includes ---
#include "cieutils/packages/concurrency/inc/ThreadStorage.hpp"
#include "cieutils/packages/concurrency/inc/ThreadPoolBase.hpp"
#include "cieutils/packages/stl_extension/inc/DynamicArray.hpp"


namespace cie::mp {


template <class TStorage>
class ThreadPoolStorage
{
private:
    using StorageContainer = DynamicArray<TStorage>;

public:
    using value_type = typename StorageContainer::value_type;

    using size_type = typename StorageContainer::size_type;

    using iterator = typename StorageContainer::iterator;

    using const_iterator = typename StorageContainer::const_iterator;

public:
    ThreadPoolStorage() = default;

    ThreadPoolStorage(ThreadPoolBase::SharedPointer p_pool)
        : _p_pool(p_pool),
          _storage(p_pool ? p_pool->size() : 0)
    {}

    ThreadPoolStorage(ThreadPoolBase::SharedPointer p_pool,
                      TStorage&& r_storage)
        : _p_pool(p_pool),
          _storage(p_pool ? p_pool->size() : 0, std::move(r_storage))
    {}

    ThreadPoolStorage(ThreadPoolStorage&& r_rhs) = default;

    ThreadPoolStorage(const ThreadPoolStorage& r_rhs) = default;

    ThreadPoolStorage& operator=(ThreadPoolStorage&& r_rhs) = default;

    ThreadPoolStorage& operator=(const ThreadPoolStorage& r_rhs) = default;

    TStorage& getLocalStorage();

    const TStorage& getLocalStorage() const;

    void setLocalStorage(TStorage&& r_storage);

    void setLocalStorage(const TStorage& r_storage);

    //value_type& at(size_type index)
    //{return _storage.at(index);}

    const value_type& at(size_type index) const
    {return _storage.at(index);}

    //value_type& operator[](size_type index)
    //{return _storage[index];}

    const value_type& operator[](size_type index) const
    {return _storage[index];}

    Size size() const
    {return _storage.size();}

    //typename StorageContainer::iterator begin()
    //{return _storage.begin();}

    const_iterator begin() const
    {return _storage.begin();}

    //typename StorageContainer::iterator end()
    //{return _storage.end();}

    const_iterator end() const
    {return _storage.end();}

private:
    ThreadPoolBase::SharedPointer _p_pool;

    StorageContainer _storage;
}; // class ThreadPoolStorage


} // namespace cie::mp

#include "cieutils/packages/concurrency/impl/ThreadPoolStorage_impl.hpp"

#endif