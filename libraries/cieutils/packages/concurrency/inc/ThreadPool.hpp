#ifndef CIE_UTILS_THREAD_POOL_HPP
#define CIE_UTILS_THREAD_POOL_HPP

// --- Utility Includes ---
#include "cieutils/packages/concurrency/inc/ThreadPoolBase.hpp"
#include "cieutils/packages/concurrency/inc/ThreadPoolStorage.hpp"
#include "cieutils/packages/macros/inc/typedefs.hpp"
#include "cieutils/packages/compile_time/packages/concepts/inc/basic_concepts.hpp"


namespace cie::mp {


template <class TStorage = ThreadStorage<Size>>
class ThreadPool final
{
public:
    using JobType = typename TStorage::template Function<void>;

    using ConstJobType = typename TStorage::template ConstFunction<void>;

    CIE_DEFINE_CLASS_POINTERS(ThreadPool)

public:
    ThreadPool();

    ThreadPool(ThreadPoolBase::SharedPointer p_pool);

    ThreadPool(TStorage&& r_storage);

    ThreadPool(const TStorage& r_storage);

    ThreadPool(ThreadPoolBase::SharedPointer p_pool,
               TStorage&& r_storage);

    ThreadPool(ThreadPoolBase::SharedPointer p_pool,
               const TStorage& r_storage);

    ThreadPool(ThreadPool&& r_rhs) = default;

    ThreadPool(const ThreadPool& r_rhs) = default;

    ThreadPool& operator=(ThreadPool&& r_rhs) = default;

    ThreadPool& operator=(const ThreadPool& r_rhs) = default;

    static Size maxNumberOfThreads();

    //void queueJob(JobType&& r_job);

    void queueJob(const JobType& r_job);

    //void queueJob(ConstJobType&& r_job);

    //void queueJob(const ConstJobType& r_job);

    Size size() const;

    Size numberOfJobs() const;

    void barrier();

    void terminate();

    ThreadPoolStorage<TStorage>& getStorage();

    const ThreadPoolStorage<TStorage>& getStorage() const;

private:
    ThreadPoolBase::SharedPointer _p_pool;

    ThreadPoolStorage<TStorage> _storage;
}; // class ThreadPool


} // namespace cie::mp


namespace cie::concepts {

template <class T>
concept ThreadPool
= requires (typename std::decay<T>::type instance)
{
    typename T::JobType;

    typename T::ConstJobType;

    {instance.queueJob(typename T::JobType())};

    {instance.barrier()};

    {instance.terminate()};
}; // concept ThreadPool


template <class T>
concept ThreadPoolPtr
=  Pointer<T>
&& ThreadPool<typename std::decay<T>::type::element_type>;

} // namespace cie::concepts

#include "cieutils/packages/concurrency/impl/ThreadPool_impl.hpp"

#endif