#ifndef CIEUTILS_CMAKE_VARIABLES_HPP
#define CIEUTILS_CMAKE_VARIABLES_HPP

// --- STL Includes ---
#include <filesystem>
#include <string>


namespace cie {

const std::filesystem::path getInstallPath();

const std::filesystem::path getBinaryPath();

const std::filesystem::path getSourcePath();

const std::filesystem::path getBuildDataPath();

const std::filesystem::path getDataPath();

const std::filesystem::path getOutputPath();

const std::filesystem::path getDebugOutputPath();

const std::filesystem::path getTestOutputPath();

const bool isOpenMPEnabled();

// TODO
//std::string getCommitHash();


} // namespace cie

#endif