#ifndef CIE_UTILS_STL_EXTENSION_EXTERNAL_HPP
#define CIE_UTILS_STL_EXTENSION_EXTERNAL_HPP

#include "cieutils/packages/stl_extension/inc/back_insert_iterator.hpp"
#include "cieutils/packages/stl_extension/inc/state_iterator.hpp"
#include "cieutils/packages/stl_extension/inc/resize.hpp"
#include "cieutils/packages/stl_extension/inc/make_shared_from_tuple.hpp"

#include "cieutils/packages/stl_extension/inc/StaticArray.hpp"
#include "cieutils/packages/stl_extension/inc/DynamicArray.hpp"
#include "cieutils/packages/stl_extension/inc/MarchingContainer.hpp"

#endif