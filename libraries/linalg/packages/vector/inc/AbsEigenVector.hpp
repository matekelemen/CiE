#ifndef CIE_LINALG_ABS_EIGEN_VECTOR_HPP
#define CIE_LINALG_ABS_EIGEN_VECTOR_HPP

// --- External Includes ---
#include <Eigen/Core>

// --- Utility Includes ---
#include "cieutils/packages/types/inc/types.hpp"
#include "cieutils/packages/compile_time/packages/concepts/inc/container_concepts.hpp"

// --- STL Includes ---
#include <ostream>


namespace cie::linalg {


/// Nothing to do with mathematical eigenvectors
template <class EigenType, class BaseType>
class AbsEigenVector
{
public:
    using eigen_type     = EigenType;
    using base_type      = BaseType;

    using value_type     = typename base_type::value_type;
    using size_type      = Size;
    using iterator       = typename base_type::iterator;
    using const_iterator = typename base_type::const_iterator;

    static const int RowTag    = EigenType::RowsAtCompileTime;
    static const int ColumnTag = EigenType::ColsAtCompileTime;

public:
    AbsEigenVector(BaseType& r_wrapped)
    requires concepts::ConstructibleFrom<EigenType, value_type*, size_type>;

    AbsEigenVector(BaseType& r_wrapped)
    requires (!concepts::ConstructibleFrom<EigenType, value_type*, size_type>);

    AbsEigenVector(BaseType& r_wrapped, size_type size)
    requires concepts::ConstructibleFrom<EigenType, value_type*, size_type>;

    AbsEigenVector(AbsEigenVector<EigenType, BaseType>&& r_rhs) = default;

    AbsEigenVector<EigenType,BaseType>& operator=(AbsEigenVector<EigenType,BaseType>&& r_rhs) = default;

    virtual ~AbsEigenVector() {static_assert(concepts::STLContainer<AbsEigenVector>);}

    value_type& at(size_type index)
    requires concepts::NonConst<BaseType>;

    value_type& operator[](size_type index)
    requires concepts::NonConst<BaseType>;

    iterator begin()
    requires concepts::NonConst<BaseType>;

    iterator end()
    requires concepts::NonConst<BaseType>;

    value_type at(size_type index) const;

    value_type operator[](size_type index) const;

    const_iterator begin() const;

    const_iterator end() const;

    size_type size() const;

    eigen_type& wrapped()
    requires concepts::NonConst<BaseType>;

    const eigen_type& wrapped() const;

    operator const eigen_type&() const;

    BaseType& base()
    requires concepts::NonConst<BaseType>;

    const BaseType& base() const;

    operator const BaseType&() const;

protected:
    void updateEigen()
    requires concepts::ConstructibleFrom<EigenType, value_type*, size_type>;

    void updateEigen()
    requires (!concepts::ConstructibleFrom<EigenType, value_type*, size_type>);

private:
    AbsEigenVector() = delete;

    AbsEigenVector(const AbsEigenVector<EigenType, BaseType>& r_rhs) = delete;

private:
    BaseType&  _r_base;
    eigen_type _eigenVector;
}; // class AbsEigenVector


template <class EigenType, class BaseType>
std::ostream& operator<<(std::ostream& r_stream, const AbsEigenVector<EigenType,BaseType>& r_vector);


} // namespace cie::linalg


namespace cie::concepts {
template <class T>
concept EigenVector
=  NumericContainer<T>
&& requires (T instance)
{
    typename T::eigen_type;
};
} // namespace cie::concepts


#include "linalg/packages/vector/impl/AbsEigenVector_impl.hpp"

#endif