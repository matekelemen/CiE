#ifndef CIE_LINALG_VECTOR_EIGEN_ARRAY_HPP
#define CIE_LINALG_VECTOR_EIGEN_ARRAY_HPP

// --- External Includes ---
#include <Eigen/Dense>

// --- Utility Includes ---
#include "cieutils/packages/compile_time/packages/concepts/inc/container_concepts.hpp"

// --- Internal Includes ---
#include "linalg/packages/vector/inc/AbsEigenVector.hpp"

// --- STL Includes ---
#include <array>


namespace cie::linalg {


template <class ValueType, int ArraySize>
class EigenArray : public AbsEigenVector< Eigen::Map<Eigen::Matrix<ValueType,ArraySize,1>>,
                                          std::array<ValueType,ArraySize> >
{
private:
    using container_type = std::array<ValueType,ArraySize>;
    using base_type = AbsEigenVector< Eigen::Map<Eigen::Matrix<ValueType,ArraySize,1>>,
                                      container_type >;

public:
    static const Size array_size = ArraySize;
    using typename base_type::eigen_type;
    using typename base_type::value_type;
    using typename base_type::size_type;

public:
    EigenArray();

    EigenArray(const std::initializer_list<value_type>&& r_components);

    EigenArray(container_type&& r_array);

    EigenArray(const container_type& r_array);

    EigenArray(const eigen_type& r_eigenArray);

    EigenArray(EigenArray<ValueType,ArraySize>&& r_rhs);

    EigenArray(const EigenArray<ValueType,ArraySize>& r_rhs);

    template <class ContainerType>
    requires concepts::ClassContainer<ContainerType, ValueType>
    EigenArray(const ContainerType& r_components);

    EigenArray<ValueType,ArraySize>& operator=(EigenArray<ValueType,ArraySize>&& r_rhs);

    EigenArray<ValueType,ArraySize>& operator=(const EigenArray<ValueType,ArraySize>& r_rhs);

private:
    container_type _container;
}; // class EigenArray


} // namespace cie::linalg

#include "linalg/packages/vector/impl/EigenArray_impl.hpp"

#endif