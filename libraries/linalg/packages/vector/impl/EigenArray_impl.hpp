#ifndef CIE_LINALG_VECTOR_EIGEN_ARRAY_IMPL_HPP
#define CIE_LINALG_VECTOR_EIGEN_ARRAY_IMPL_HPP

// --- Utility Includes ---
#include "cieutils/packages/macros/inc/checks.hpp"

// --- STL Includes ---
#include <algorithm>


namespace cie::linalg {


template <class ValueType, int ArraySize>
EigenArray<ValueType,ArraySize>::EigenArray()
    : EigenArray<ValueType,ArraySize>::base_type( _container ),
      _container()
{
    this->updateEigen();
}


template <class ValueType, int ArraySize>
EigenArray<ValueType,ArraySize>::EigenArray(const std::initializer_list<typename EigenArray<ValueType,ArraySize>::value_type>&& r_components)
    : EigenArray<ValueType,ArraySize>::base_type( _container )
{
    std::copy(
        r_components.begin(),
        r_components.end(),
        _container.begin()
    );
    this->updateEigen();
}


template <class ValueType, int ArraySize>
EigenArray<ValueType,ArraySize>::EigenArray( const typename EigenArray<ValueType,ArraySize>::eigen_type& r_eigenArray )
    : EigenArray<ValueType,ArraySize>::base_type( _container ),
      _container()
{
    std::copy(
        r_eigenArray.begin(),
        r_eigenArray.end(),
        _container.begin()
    );
    this->updateEigen();
}


template <class ValueType, int ArraySize>
EigenArray<ValueType,ArraySize>::EigenArray( typename EigenArray<ValueType,ArraySize>::container_type&& r_array )
    : EigenArray<ValueType,ArraySize>::base_type( _container ),
      _container( std::move(r_array) )
{
    this->updateEigen();
}


template <class ValueType, int ArraySize>
EigenArray<ValueType,ArraySize>::EigenArray( const typename EigenArray<ValueType,ArraySize>::container_type& r_array )
    : EigenArray<ValueType,ArraySize>::base_type( _container ),
      _container( r_array )
{
    this->updateEigen();
}


template <class ValueType, int ArraySize>
EigenArray<ValueType,ArraySize>::EigenArray( EigenArray<ValueType,ArraySize>&& r_rhs )
    : EigenArray<ValueType,ArraySize>::base_type( _container ),
      _container( std::move(r_rhs._container) )
{
    this->updateEigen();
}


template <class ValueType, int ArraySize>
EigenArray<ValueType,ArraySize>::EigenArray( const EigenArray<ValueType,ArraySize>& r_rhs )
    : EigenArray<ValueType,ArraySize>::base_type( _container ),
      _container( r_rhs._container )
{
    this->updateEigen();
}


template <class ValueType, int ArraySize>
template <class ContainerType>
requires concepts::ClassContainer<ContainerType, ValueType>
EigenArray<ValueType,ArraySize>::EigenArray( const ContainerType& r_components )
    : EigenArray<ValueType,ArraySize>::base_type( _container ),
      _container()
{
    CIE_OUT_OF_RANGE_CHECK( r_components.size() == this->size() )
    std::copy(
        r_components.begin(),
        r_components.end(),
        this->begin()
    );
    this->updateEigen();
}


template <class ValueType, int ArraySize>
inline EigenArray<ValueType,ArraySize>&
EigenArray<ValueType,ArraySize>::operator=( EigenArray<ValueType,ArraySize>&& r_rhs )
{
    _container = std::move( r_rhs._container );
    this->updateEigen();

    return *this;
}


template <class ValueType, int ArraySize>
inline EigenArray<ValueType,ArraySize>&
EigenArray<ValueType,ArraySize>::operator=( const EigenArray<ValueType,ArraySize>& r_rhs )
{
    _container = r_rhs._container;
    this->updateEigen();

    return *this;
}


} // namespace cie::linalg


#endif