#ifndef CIE_CSG_TREES_CARTESIAN_GRID_SAMPLER_IMPL_HPP
#define CIE_CSG_TREES_CARTESIAN_GRID_SAMPLER_IMPL_HPP

// --- Utility Includes ---
#include "cieutils/packages/stl_extension/inc/zip.hpp"
#include "cieutils/packages/macros/inc/exceptions.hpp"


namespace cie::csg {


template <concepts::Primitive PrimitiveType>
AbsCartesianGridSampler<PrimitiveType>::AbsCartesianGridSampler(Size numberOfPointsPerDimension) :
    _p_indexConverter(new CartesianIndexConverter<PrimitiveType::dimension>(numberOfPointsPerDimension)),
    _numberOfPointsPerDimension(numberOfPointsPerDimension)
{
    this->setNumberOfPointsPerDimension(numberOfPointsPerDimension);
}


template <concepts::Primitive PrimitiveType>
inline Size
AbsCartesianGridSampler<PrimitiveType>::size() const
{
    return this->_size;
}


template <concepts::Primitive PrimitiveType>
inline Size
AbsCartesianGridSampler<PrimitiveType>::numberOfPointsPerDimension() const
{
    return this->_numberOfPointsPerDimension;
}


template <concepts::Primitive PrimitiveType>
void
AbsCartesianGridSampler<PrimitiveType>::setNumberOfPointsPerDimension(Size numberOfPointsPerDimension)
{
    CIE_BEGIN_EXCEPTION_TRACING

    this->_numberOfPointsPerDimension = numberOfPointsPerDimension;
    this->_size = intPow(this->_numberOfPointsPerDimension, PrimitiveType::dimension);
    _p_indexConverter.reset(new CartesianIndexConverter<PrimitiveType::dimension>(numberOfPointsPerDimension));

    CIE_END_EXCEPTION_TRACING
}


template <concepts::Primitive PrimitiveType>
const CartesianIndexConverter<PrimitiveType::dimension>&
AbsCartesianGridSampler<PrimitiveType>::indexConverter() const
{
    return *this->_p_indexConverter;
}


// ---------------------------------------------------------
// SPECIALIZED PRIMITIVE SAMPLERS
// ---------------------------------------------------------

/* --- Cube sampler --- */

template <concepts::Cube PrimitiveType>
CartesianGridSampler<PrimitiveType>::CartesianGridSampler(Size numberOfPointsPerDimension) :
    AbsCartesianGridSampler<PrimitiveType>(numberOfPointsPerDimension)
{
}


template <concepts::Cube PrimitiveType>
inline typename CartesianGridSampler<PrimitiveType>::point_type
CartesianGridSampler<PrimitiveType>::getSamplePoint(const PrimitiveType& r_primitive,
                                                     Size index) const
{
    CIE_BEGIN_EXCEPTION_TRACING

    using CoordinateType = typename CartesianGridSampler<PrimitiveType>::coordinate_type;

    typename CartesianGridSampler<PrimitiveType>::point_type point;

    if (this->numberOfPointsPerDimension() == 1) // center point
    {
        constexpr CoordinateType denominator = 2;
        for (auto&& [r_component, r_base] : utils::zip(point, r_primitive.base()))
            r_component = r_base + r_primitive.length() / denominator;
    }
    else
    {
        const CoordinateType coefficient = r_primitive.length() / (this->numberOfPointsPerDimension() - 1);

        for (auto&& [r_component, r_index, r_base] : utils::zip(point, this->indexConverter().convert(index), r_primitive.base()))
            r_component = r_base + r_index * coefficient;
    }

    return point;

    CIE_END_EXCEPTION_TRACING
}



/* --- Box sampler --- */

template <concepts::Box PrimitiveType>
CartesianGridSampler<PrimitiveType>::CartesianGridSampler(Size numberOfPointsPerDimension) :
    AbsCartesianGridSampler<PrimitiveType>(numberOfPointsPerDimension)
{
}

template <concepts::Box PrimitiveType>
inline typename CartesianGridSampler<PrimitiveType>::point_type
CartesianGridSampler<PrimitiveType>::getSamplePoint(const PrimitiveType& r_primitive,
                                                    Size index) const
{
    CIE_BEGIN_EXCEPTION_TRACING

    using CoordinateType = typename CartesianGridSampler<PrimitiveType>::coordinate_type;
    typename CartesianGridSampler<PrimitiveType>::point_type point;

    if (this->numberOfPointsPerDimension() == 1) // center point
    {
        constexpr CoordinateType denominator = 2;
        for (auto&& [r_component, r_base, r_length] : utils::zip(point, r_primitive.base(), r_primitive.lengths()))
            r_component = r_base + r_length / denominator;
    }
    else
    {
        const CoordinateType denominator = this->numberOfPointsPerDimension() - 1;
        for (auto&& [r_component, r_index, r_base, r_length] : utils::zip(point, this->indexConverter().convert(index), r_primitive.base(), r_primitive.lengths()))
            r_component = r_base + r_index * r_length / denominator;
    }

    return point;

    CIE_END_EXCEPTION_TRACING
}


} // namespace cie::csg


#endif