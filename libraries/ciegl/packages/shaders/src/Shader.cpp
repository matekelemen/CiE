// --- Utility Includes ---
#include "cieutils/packages/macros/inc/exceptions.hpp"
#include "cieutils/packages/io/inc/json.hpp"

// --- Internal Includes ---
#include "ciegl/packages/shaders/inc/Shader.hpp"

// --- STL Includes ---
#include <exception>
#include <fstream>
#include <sstream>
#include <limits>


namespace cie::gl {


Shader::Shader( const std::filesystem::path& r_configPath,
                const std::filesystem::path& r_codePath ) :
    utils::IDObject<Size>( std::numeric_limits<Size>().max() )
{
    CIE_BEGIN_EXCEPTION_TRACING

    {
        io::JSONObject configuration(r_configPath);

        // Parse config file - attributes
        {
            auto attributes = configuration["attributes"];
            auto it_end = attributes.end();
            for (auto it=attributes.begin(); it!=it_end; ++it)
            {
                auto item = *it;
                _attributes.emplace_back(
                    it.key(),
                    item["size"].as<Size>(),
                    item["stride"].as<Size>(),
                    item["offset"].as<Size>()
                );
            }
        }

        // Parse config file - uniforms
        {
            auto uniforms = configuration["uniforms"];
            const auto it_end = uniforms.end();
            for (auto it=uniforms.begin(); it!=it_end; ++it)
            {
                auto item = *it;
                _uniforms.emplace_back(
                    it.key(),
                    item["size"].as<Size>(),
                    item["type"].as<std::string>()
                );
            }
        }

        // Parse config file - textures
        {
            auto textures = configuration["textures"];
            auto it_end = textures.end();
            for (auto it=textures.begin(); it!=it_end; ++it)
            {
                auto item = *it;

                Size channels = 3;
                if (item.hasKey("channels"))
                    channels = item["channels"].as<Size>();

                _textures.emplace_back(
                    it.key(),
                    item["dimension"].as<Size>(),
                    channels,
                    item["type"].as<std::string>()
                );
            }
        }

        // Parse config file - outputs
        {
            auto outputs = configuration["outputs"];
            auto it_end = outputs.end();
            Size index = 0;
            for (auto it=outputs.begin(); it!=it_end; ++it)
            {
                _outputs.emplace_back(
                    it.key(),
                    index++
                );
            }
        }
    }

    // Load shader code
    std::ifstream source( r_codePath );
    if (!source.is_open())
        CIE_THROW(Exception, "Unable to open shader source file: " + r_codePath.string())

    std::stringstream stream;
    stream << source.rdbuf();
    _source = stream.str();

    CIE_END_EXCEPTION_TRACING
}


void Shader::print( std::ostream& r_stream ) const
{
    CIE_BEGIN_EXCEPTION_TRACING

    r_stream << this->_source << std::endl;

    CIE_END_EXCEPTION_TRACING
}


const std::string& Shader::source() const
{
    return _source;
}


const typename Shader::attribute_container& Shader::attributes() const
{
    return _attributes;
}


const typename Shader::uniform_container& Shader::uniforms() const
{
    return _uniforms;
}


const typename Shader::texture_container& Shader::textures() const
{
    return _textures;
}


const typename Shader::output_container& Shader::outputs() const
{
    return _outputs;
}


} // namespace cie::gl