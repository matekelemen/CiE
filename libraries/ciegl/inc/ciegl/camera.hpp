#ifndef CIE_GL_CAMERA_HPP_EXTERNAL
#define CIE_GL_CAMERA_HPP_EXTERNAL

#include "ciegl/packages/camera/inc/OrthographicCamera.hpp"
#include "ciegl/packages/camera/inc/PerspectiveCamera.hpp"

#endif