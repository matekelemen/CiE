# Python modules
set(CIE_BUILD_PYTHON_MODULES ON CACHE BOOL "Build and install python modules and their dependencies")
if (${CIE_BUILD_PYTHON_MODULES})
    set(CIE_PYTHON_MODULE_BUILD_DIR "${CMAKE_BINARY_DIR}/modules")
    set(CIE_PYTHON_MODULE_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}")
endif()

# Binary directories
set(CIE_BUILD_EXECUTABLE_DIR "${CMAKE_BINARY_DIR}/bin" CACHE STRING "Build directory of executables")

if (UNIX)
    set(CIE_BUILD_LIBRARY_DIR "${CMAKE_BINARY_DIR}/lib" CACHE STRING "Build directory of libraries")
elseif (WIN32)
    set(CIE_BUILD_LIBRARY_DIR "${CMAKE_BINARY_DIR}/bin" CACHE STRING "Build directory of libraries")
endif()

set(CIE_INSTALL_EXECUTABLE_DIR "${CMAKE_INSTALL_PREFIX}" CACHE STRING "Install directory of executables")
set(CIE_INSTALL_LIBRARY_DIR "${CMAKE_INSTALL_PREFIX}" CACHE STRING "Install directory of libraries")

# Data directories
set(CIE_BUILD_DATA_DIR "${CMAKE_BINARY_DIR}/data" CACHE STRING "Copy data to this directory during the build process")

if (UNIX)
    set(CIE_INSTALL_DATA_DIR "$ENV{HOME}/.local/share/CiE" CACHE STRING "Data will be copied to this directory at install")
elseif(WIN32)
    set(CIE_INSTALL_DATA_DIR "$ENV{HOMEPATH}/Documents/CiE" CACHE STRING "Data will be copied to this directory at install")
endif()

# Python directories
set(CIE_BUILD_PYTHON_MODULE_DIR "${CMAKE_BINARY_DIR}/modules" CACHE STRING "Copy python modules to this directory after compiling.")
set(CIE_INSTALL_PYTHON_MODULE_DIR "${CMAKE_INSTALL_PREFIX}/cie_python_modules" CACHE STRING "Install python modules to this directory.")

# Script directories
set(CIE_BUILD_SCRIPT_DIR "${CMAKE_BINARY_DIR}/scripts" CACHE STRING "Copy scripts to this directory after compiling.")
set(CIE_INSTALL_SCRIPT_DIR "${CMAKE_INSTALL_PREFIX}/cie_scripts" CACHE STRING "Install scripts to this directory.")

# Runtime directories
set(CIE_OUTPUT_DIR "${CIE_INSTALL_DATA_DIR}/output" CACHE STRING "Logs and other output files will be written to this directory.")
set(CIE_DEBUG_OUTPUT_DIR "${CIE_INSTALL_DATA_DIR}/debug_output" CACHE STRING "Debug logs will be dumped to this directory.")
set(CIE_TEST_OUTPUT_DIR "${CIE_INSTALL_DATA_DIR}/test_output" CACHE STRING "Test logs will be dumped to this directory.")

# Select what to build (libraries, application, tests)
get_cie_libraries(library_names)
foreach(target_name ${library_names})
    set(CIE_BUILD_LIBRARY_${target_name} ON CACHE BOOL "build library: ${target_name}")
endforeach()

get_cie_applications(application_names)
foreach(target_name ${application_names})
    set(CIE_BUILD_APPLICATION_${target_name} ON CACHE BOOL "build application: ${target_name}")
endforeach()

set(CIE_BUILD_TESTS OFF CACHE BOOL "Build testrunners for all libraries")

# OpenMP
set(CIE_ENABLE_OPENMP OFF CACHE BOOL "Enable OpenMP shared memory parallelization")
if (${CIE_ENABLE_OPENMP})
    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")
    elseif(CMAKE_CXX_COMPILER_ID STREQUAL MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /openmp")
    endif()
endif()

# Graphics
set(OpenGL_GL_PREFERENCE "GLVND" CACHE STRING "GLVND or LEGACY")

# Enable/Disable macros
set(CIE_ENABLE_EXCEPTION_TRACING ON CACHE BOOL "Trace thrown exceptions through functions")
if (${CIE_ENABLE_EXCEPTION_TRACING})
    add_compile_definitions(CIE_ENABLE_EXCEPTION_TRACING)
endif()

set(CIE_ENABLE_OUT_OF_RANGE_TESTS ON CACHE BOOL "")
if(${CIE_ENABLE_OUT_OF_RANGE_TESTS})
    add_compile_definitions(CIE_ENABLE_OUT_OF_RANGE_TESTS)
endif()

set(CIE_ENABLE_DIVISION_BY_ZERO_CHECKS ON CACHE BOOL "")
if(${CIE_ENABLE_DIVISION_BY_ZERO_CHECKS})
    add_compile_definitions(CIE_ENABLE_DIVISION_BY_ZERO_CHECKS)
endif()

set(CIE_ENABLE_RUNTIME_GEOMETRY_CHECKS ON CACHE BOOL "")
if(${CIE_ENABLE_RUNTIME_GEOMETRY_CHECKS})
    add_compile_definitions(CIE_ENABLE_RUNTIME_GEOMETRY_CHECKS)
endif()

set(CIE_ENABLE_DEBUG_FILE_OUTPUT ON CACHE BOOL "Enable writing debug info to files")
if (${CIE_ENABLE_DEBUG_FILE_OUTPUT})
    add_compile_definitions(CIE_ENABLE_DEBUG_FILE_OUTPUT)
endif()
