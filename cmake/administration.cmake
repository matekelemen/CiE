# Project layout variables
set(CIE_LIBRARY_SOURCE_PATHS "" CACHE STRING "List of paths pointing to library sources")
set(CIE_APPLICATION_SOURCE_PATHS "" CACHE STRING "List of paths pointing to application sources")


# Add local layout
list_subdirectories(cie_local_libraries "${CIE_SOURCE_DIR}/libraries")
list(APPEND CIE_LIBRARY_SOURCE_PATHS ${cie_local_libraries})

list_subdirectories(cie_local_applications "${CIE_SOURCE_DIR}/applications")
list(APPEND CIE_APPLICATION_SOURCE_PATHS ${cie_local_applications})


# Misc python
macro(get_python_binding_name binding_name)
    set(${binding_name} "py${PROJECT_NAME}")
endmacro()


# Get name from path
macro(get_library_name output_name input_path)
    get_filename_component(${output_name} "${input_path}" NAME)
endmacro()

macro(get_application_name output_name input_path)
    get_filename_component(${output_name} "${input_path}" NAME)
endmacro()

macro(get_python_module_name output_name input_path)
    get_filename_component(${output_name} "${input_path}" NAME)
endmacro()


# Get path from name
macro(get_python_module_build_path module_path module_name)
    set(${module_path} "${CIE_BUILD_PYTHON_MODULE_DIR}/${module_name}")
endmacro()

macro(get_python_module_install_path module_path module_name)
    set(${module_path} "${CIE_INSTALL_PYTHON_MODULE_DIR}/${module_name}")
endmacro()


# Get all library/application paths
macro(get_cie_library_paths output_paths)
    list(APPEND ${output_paths} ${CIE_LIBRARY_SOURCE_PATHS})
endmacro()

macro(get_cie_application_paths output_paths)
    list(APPEND ${output_paths} ${CIE_APPLICATION_SOURCE_PATHS})
endmacro()


# Library and executable names
macro(get_cie_libraries output_names)
    get_cie_library_paths(pths)
    foreach(pth ${pths})
        get_library_name(target_name "${pth}")
        list(APPEND ${output_names} "${target_name}")
    endforeach()
endmacro()

macro(get_cie_applications output_names)
    get_cie_application_paths(pths)
    foreach(pth ${pths})
        get_application_name(target_name "${pth}")
        list(APPEND ${output_names} "${target_name}")
    endforeach()
endmacro()
