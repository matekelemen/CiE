# Collect all subdirectories in a directory
macro(list_subdirectories result directory)
    file(GLOB children "${directory}/*")
    foreach(child ${children})
        if (IS_DIRECTORY "${child}")
            list(APPEND ${result} "${child}")
        endif()
    endforeach()
endmacro()


# Collect all files in a directory
macro(list_files result directory)
    file(GLOB children "${directory}/*")
    foreach(child ${children})
        if (NOT IS_DIRECTORY "${child}")
            list(APPEND ${result} "${child}")
        endif()
    endforeach()
endmacro()


# Create a directory if it doesn't exist (or throw an error if it's a file)
function(ensure_directory directory)
    if (EXISTS "${directory}")
        if (NOT IS_DIRECTORY "${directory}")
            message(FATAL_ERROR "${directory} is a file")
        endif()
    else()
        file(MAKE_DIRECTORY "${directory}")
    endif()
endfunction()


# Copy a file to the destination directory
function(copy_file file_path destination)
    ensure_directory("${destination}")
    get_filename_component(file_name "${file_path}" NAME)
    configure_file(${file_path} "${destination}/${file_name}" COPYONLY)
endfunction()


# Copy items from a directory
function(copy_directory_contents directory destination)
    ensure_directory("${destination}")
    file(COPY "${directory}/" DESTINATION "${destination}")
endfunction()


# Copy a directory to the destination directory
function(copy_directory directory destination)
    ensure_directory("${destination}")
    file(COPY "${directory}" DESTINATION "${destination}")
endfunction()


# Copy a target to the destination directory
function(copy_target target destination)
    ensure_directory("${destination}")
    set(target_path $<TARGET_FILE:${target}>)
    get_filename_component(target_name "${target_path}" NAME)
    add_custom_command(TARGET ${target} POST_BUILD
                       COMMAND ${CMAKE_COMMAND} -E copy
                       "${target_path}" "${destination}/")
endfunction()


# Recursively copy all linked libraries of a target to a destination directory
function(copy_linked_libraries target destination)
    get_target_property(dependency ${target} LINK_LIBRARIES)
    foreach(dependency ${dependencies})
        copy_target(${dependency} "${destination}")
        copy_linked_libraries(${dependency} "${destination}")
    endforeach()
endfunction()


# Install a file to the destination directory
function(install_file file_path destination)
    install(FILES "${file_path}" DESTINATION "${destination}")
endfunction()


# Install a directory to the destidnation directory
function(install_directory directory destination)
    get_filename_component(directory_name "${directory}" NAME)
    install(DIRECTORY "${directory}/" DESTINATION "${destination}/${directory_name}/")
endfunction()


# Define the output file path of a binary
function(set_library_output_path target destination)
    set_target_properties(${target} PROPERTIES LIBRARY_OUTPUT_DIRECTORY "${destination}")
endfunction()


# Define the output file path of a binary
function(set_executable_output_path target destination)
    set_target_properties(${target} PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${destination}")
endfunction()


# Install a library to the destination directory
function(install_library_to target destination)
    if (UNIX)
        install(TARGETS ${target} LIBRARY DESTINATION "${destination}")
    elseif (WIN32)
        install(TARGETS ${target} RUNTIME DESTINATION "${destination}")
    endif()
endfunction()


# Install an executable to the destination directory
function(install_executable_to target destination)
    install(TARGETS ${target} RUNTIME DESTINATION "${destination}")
endfunction()


# Link a target binary to all specified libraries
function(link target)
    if (TARGET ${target})
        # Remove 'target' from the rest of the arguments
        list(REMOVE_ITEM ARGV ${target})

        # Link and a bit of stackoverflow magic
        target_link_libraries(${target} PUBLIC ${ARGV})
        set_target_properties(${target} PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE)
    endif()
endfunction()
