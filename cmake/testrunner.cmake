set(test_dir ${VAR})
message(STATUS "Run all tests in ${test_dir}")
file(GLOB test_executables ${test_dir}/*_testrunner*)

foreach(test_executable ${test_executables})
    message(STATUS "running ${test_executable}")
    execute_process(COMMAND "${test_executable}"
                    ENCODING AUTO)
endforeach()
