# Collect package files recursively
# Expects the following directory layout:
# - package_name
#       - inc
#       - impl
#       - src
#       - test
#       - [packages] (optional, with identical substructure)
macro(collect_package directory headers sources tests)
    set(temp_headers "")
    set(temp_impl "")
    set(temp_sources "")
    set(temp_tests "")

    FILE(GLOB temp_headers ${directory}/inc/*.hpp)
    FILE(GLOB temp_impl ${directory}/impl/*.hpp)
    FILE(GLOB temp_sources ${directory}/src/*.cpp)
    FILE(GLOB temp_tests ${directory}/test/*.cpp)

    list(APPEND ${headers} ${temp_headers})
    list(APPEND ${headers} ${temp_impl})
    list(APPEND ${sources} ${temp_sources})
    list(APPEND ${tests} ${temp_tests})

    if(EXISTS "${directory}/packages")
        list_subdirectories(_package_paths "${directory}/packages")
        foreach(_package_path ${_package_paths})
            collect_package("${_package_path}"
                            package_headers
                            package_sources
                            package_tests)

            list(APPEND ${headers} ${package_headers})
            list(APPEND ${sources} ${package_sources})
            list(APPEND ${tests} ${package_tests})
        endforeach()
    endif()
endmacro()


# No recursive calls
macro(collect_packages headers sources tests)
    set(headers "")
    set(sources "")
    set(tests "")

    list_subdirectories(package_paths "${CMAKE_CURRENT_SOURCE_DIR}/packages")

    foreach(package_path ${package_paths})
        collect_package("${package_path}"
                        _headers _sources _tests)
        LIST(APPEND ${headers} ${_headers})
        LIST(APPEND ${sources} ${_sources})
        LIST(APPEND ${tests} ${_tests})
    endforeach()
endmacro()