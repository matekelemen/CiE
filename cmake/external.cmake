# Testing
if (${CIE_BUILD_TESTS})
    find_package(Catch2 REQUIRED)
endif()

# Linear algebra
if (${CIE_BUILD_LIBRARY_linalg})
    find_package(Eigen3 REQUIRED)
    include_directories("${EIGEN3_INCLUDE_DIR}")
    include_directories("${EIGEN3_INCLUDE_DIR}/unsupported")
endif()

# Python bindings
set(PYBIND11_CPP_STANDARD "-std=c++${CMAKE_CXX_STANDARD}")
find_package(pybind11 REQUIRED)

# Graphics
find_package(OpenGL REQUIRED)

find_package(glfw3 REQUIRED)

find_package(glm REQUIRED)

# FileIO
find_package(PugiXML REQUIRED)

find_package(nlohmann_json REQUIRED)

include_directories(external/stb)
