# Build a shared library
function(build_shared_library target)
    # Remove 'target' from the rest of the arguments
    list(REMOVE_ITEM ARGV ${target})

    #message(STATUS "build shared library: ${target}")
    add_library(${target} SHARED ${ARGV})
    set_library_output_path(${target} "${CIE_BUILD_LIBRARY_DIR}")

    # Include target export includes
    target_include_directories(${target} INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/inc")
endfunction()


# Build an executable
function(build_executable target)
    # Remove 'target' from the rest of the arguments
    list(REMOVE_ITEM ARGV ${target})

    #message(STATUS "build executable: ${target}")
    add_executable(${target} ${ARGV})
    set_executable_output_path(${target} "${CIE_BUILD_EXECUTABLE_DIR}")
endfunction()


function(build_testrunner target)
    if(${CIE_BUILD_TESTS})
        build_executable(${ARGV})
        #target_include_directories(${target} PRIVATE "${CMAKE_BINARY_DIR}/generated-includes")
        #link(${target} Catch2)
    endif()
endfunction()


function(build_python_bindings)
    get_python_binding_name(binding_name)

    file(GLOB_RECURSE sources "${CMAKE_CURRENT_SOURCE_DIR}/bindings/*")
    pybind11_add_module(${binding_name} ${sources})
    link(${binding_name} ${PROJECT_NAME})
endfunction()


function(attach_python_bindings_to_module binding_library module_name)
    get_python_module_build_path(module_build_path "${module_name}")
    copy_target("${binding_library}" "${module_build_path}")

    get_python_module_install_path(module_install_path "${module_name}")
    install_library_to("${binding_library}" "${module_install_path}")
endfunction()


function(install_library target)
    install_library_to(${target} "${CIE_INSTALL_LIBRARY_DIR}")
endfunction()


function(install_executable target)
    install_executable_to(${target} "${CIE_INSTALL_EXECUTABLE_DIR}")
endfunction()


function(copy_data)
    copy_directory_contents("${CMAKE_CURRENT_SOURCE_DIR}/data" "${CIE_BUILD_DATA_DIR}/${PROJECT_NAME}")
    install_directory("${CMAKE_CURRENT_SOURCE_DIR}/data" "${CIE_INSTALL_DATA_DIR}/${PROJECT_NAME}")
endfunction()


function(copy_scripts)
    copy_directory_contents("${CMAKE_CURRENT_SOURCE_DIR}/scripts" "${CIE_BUILD_SCRIPT_DIR}/${PROJECT_NAME}")
    install_directory("${CMAKE_CURRENT_SOURCE_DIR}/scripts/" "${CIE_INSTALL_SCRIPT_DIR}/${PROJECT_NAME}/")
endfunction()


function(copy_python_modules)
    list_subdirectories(modules "${CMAKE_CURRENT_SOURCE_DIR}/modules")
    foreach(module_path ${modules})
        get_python_module_name(module_name "${module_path}")
        copy_directory("${module_path}" "${CIE_BUILD_PYTHON_MODULE_DIR}")
        install_directory("${module_path}" "${CIE_INSTALL_PYTHON_MODULE_DIR}")
    endforeach()
endfunction()
